# SmolReader

Fork of [FBReader](https://github.com/geometer/FBReaderJ), but smaller.


# Changes

Mostly, changes in this fork falls under one of theese three categories:
 - Simplify and refactor app internals;
 - Remove unwanted/unnecessary functionality;
 - Rework application interface.

For complete list of changes please see [changelog](CHANGELOG.md).


# License

This project [is licensed](LICENSE.md) under the GNU General Public License v3.0.

Original [sources](https://github.com/geometer/FBReaderJ) were [provided](https://fbreader.org/android) under GNU GPLv2 license.

