package com.somobu.smolreader.reader;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.somobu.smolreader.R;
import com.somobu.smolreader.UglyStore;
import com.somobu.smolreader.bookmarks.BookmarksActivity;
import com.somobu.smolreader.library.BookInfoActivity;
import com.somobu.smolreader.library.BookInfoFragment;
import com.somobu.smolreader.library.LibraryActivity;
import com.somobu.smolreader.prefs.PreferencesActivity;

import org.geometerplus.android.fbreader.FBReader;
import org.geometerplus.android.fbreader.FBReaderIntents;
import org.geometerplus.android.fbreader.FBUtil;
import org.geometerplus.android.util.ViewUtil;
import org.geometerplus.fbreader.book.Book;
import org.geometerplus.fbreader.book.BookUtil;
import org.geometerplus.fbreader.book.Bookmark;
import org.geometerplus.fbreader.book.BookmarkQuery;
import org.geometerplus.fbreader.book.IBookCollection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BackMenuDialogFragment extends DialogFragment {

    public static class ActionDescription {

        public final String Title;
        public final String Summary;
        public final Runnable onSelected;

        ActionDescription(String title, String summary, Runnable onSelected) {
            Title = title;
            Summary = summary;
            this.onSelected = onSelected;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.v2_dialog_backmenu, container, false);

        FBReader activity = (FBReader) getActivity();

        final Book book = activity.getCurrentBook();
        boolean bookMgmtVisible = book != null && BookUtil.fileByBook(book).getPhysicalFile() != null;

        if (bookMgmtVisible) {
            TextView bookInfo = root.findViewById(R.id.book_info);
            bookInfo.setText(book.getTitle());

            bind(root, R.id.book_info, view -> {
                Intent intent = new Intent(activity, BookInfoActivity.class);
                intent.putExtra(BookInfoFragment.KEY_BOOK_FILE, book.getPath());
                startActivity(intent);
            });

            bind(root, R.id.search, view -> {
                activity.hideBars();
                activity.onSearchRequested();
            });

            bind(root, R.id.share, view -> FBUtil.shareBook(activity, book));
        } else {
            root.findViewById(R.id.book_row).setVisibility(View.GONE);
        }

        bind(root, R.id.library, view -> startActivity(new Intent(activity, LibraryActivity.class)));
        bind(root, R.id.bookmarks, view -> {
            Intent intent = new Intent(activity, BookmarksActivity.class);
            FBReaderIntents.putBookExtra(intent, book);
            startActivity(intent);
        });
        bind(root, R.id.settings, view -> startActivity(new Intent(activity, PreferencesActivity.class)));

        bind(root, R.id.day_night, view -> activity.toggleDayNight());
        if (activity.isNightColorProfile()) {
            ImageButton btn = root.findViewById(R.id.day_night);
            btn.setImageResource(R.drawable.ic_menu_day);
        }

        bind(root, R.id.zoom_in, view -> activity.changeFontSize(+2));
        bind(root, R.id.zoom_out, view -> activity.changeFontSize(-2));

        UglyStore.local(getActivity(), store -> {
            LinearLayout ll = root.findViewById(R.id.awoo);

            List<ActionDescription> descrs = getActionsList(activity, store);
            for (ActionDescription desc : descrs) {
                View v = getView(desc, ll);
                ll.addView(v);
                v.setOnClickListener(view -> {
                    desc.onSelected.run();
                    getDialog().dismiss();
                });
            }

        });

        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        return root;
    }

    public List<ActionDescription> getActionsList(FBReader ctx, IBookCollection<Book> collection) {
        final List<ActionDescription> list = new ArrayList<>();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);

        if (prefs.getBoolean("CANCEL_MENU_PREVIOUS", true)) {
            Book previousBook = collection.getRecentBook(1);
            if (previousBook != null) {
                list.add(new ActionDescription(
                        "Open previous book",
                        previousBook.getTitle(),
                        () -> ctx.openDatBook(previousBook))
                );
            }
        }


        if (prefs.getBoolean("CANCEL_MENU_LAST_THREE", true)) {
            Book currentBook = collection.getRecentBook(0);
            if (currentBook != null) {
                BookmarkQuery bookmarkQuery = new BookmarkQuery(currentBook, false, 3);
                List<Bookmark> bookmarks = collection.bookmarks(bookmarkQuery);
                Collections.sort(bookmarks, new Bookmark.ByTimeComparator());

                for (Bookmark b : bookmarks) {
                    list.add(new ActionDescription(
                            "Return to ...",
                            b.getText(),
                            () -> ctx.backmenuGotoBookmark(b))
                    );
                }
            }
        }

        list.add(new ActionDescription("Close Smol Reader", null, ctx::finish));

        return list;
    }

    public View getView(ActionDescription item, ViewGroup parent) {
        final View view = LayoutInflater.from(getActivity()).inflate(R.layout.v2_item_backmenu, parent, false);

        final TextView titleView = ViewUtil.findTextView(view, R.id.cancel_item_title);
        final TextView summaryView = ViewUtil.findTextView(view, R.id.cancel_item_summary);

        final String title = item.Title;
        final String summary = item.Summary;

        titleView.setText(title);

        if (summary != null) {
            summaryView.setVisibility(View.VISIBLE);
            summaryView.setText(summary);
            titleView.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
            ));
        } else {
            summaryView.setVisibility(View.GONE);
            titleView.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT
            ));
        }

        return view;
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        ensureBars();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        ensureBars();
    }

    void bind(View root, int id, View.OnClickListener l) {
        root.findViewById(id).setOnClickListener(view -> {
            l.onClick(view);
            getDialog().dismiss();
        });
    }

    private void ensureBars() {
        if (getActivity() instanceof FBReader) {
            FBReader BaseActivity = (FBReader) getActivity();
            if (BaseActivity.barsAreShown()) {
                BaseActivity.showBars();
            } else {
                BaseActivity.hideBars();
            }
        }
    }
}
