package com.somobu.smolreader.reader;

import org.geometerplus.android.fbreader.FBAndroidAction;
import org.geometerplus.android.fbreader.FBReader;
import org.geometerplus.fbreader.fbreader.FBReaderApp;

public class BackMenuAction extends FBAndroidAction {

    public BackMenuAction(FBReader baseActivity, FBReaderApp fbreader) {
        super(baseActivity, fbreader);
    }

    @Override
    protected void run(Object... params) {
        BackMenuDialogFragment bmdf = new BackMenuDialogFragment();
        bmdf.show(BaseActivity.getFragmentManager(), "backdlg");
    }
}
