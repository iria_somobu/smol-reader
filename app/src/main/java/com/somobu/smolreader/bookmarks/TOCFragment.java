package com.somobu.smolreader.bookmarks;

import android.app.ListFragment;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.somobu.smolreader.R;

import org.geometerplus.android.fbreader.ZLTreeAdapter;
import org.geometerplus.android.util.ViewUtil;
import org.geometerplus.fbreader.bookmodel.TOCTree;
import org.geometerplus.fbreader.fbreader.FBReaderApp;
import org.geometerplus.zlibrary.core.application.ZLApplication;
import org.geometerplus.zlibrary.core.resources.ZLResource;
import org.geometerplus.zlibrary.core.tree.ZLTree;

public class TOCFragment extends ListFragment {

    private static final int PROCESS_TREE_ITEM_ID = 0;
    private static final int READ_BOOK_ITEM_ID = 1;

    private TOCAdapter myAdapter;
    private ZLTree<?> mySelectedItem;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final FBReaderApp fbreader = (FBReaderApp) ZLApplication.Instance();
        final TOCTree root = fbreader.Model.TOCTree;
        myAdapter = new TOCAdapter(getListView(), root);
        TOCTree treeToSelect = fbreader.getCurrentTOCElement();
        myAdapter.selectItem(treeToSelect);
        setListAdapter(myAdapter);
        mySelectedItem = treeToSelect;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        final int position = ((AdapterView.AdapterContextMenuInfo) menuInfo).position;
        final TOCTree tree = (TOCTree) getListAdapter().getItem(position);
        if (tree.hasChildren()) {
            menu.setHeaderTitle(tree.getText());
            final ZLResource resource = ZLResource.resource("tocView");
            menu.add(0, PROCESS_TREE_ITEM_ID, 0, resource.getResource(((TOCAdapter) getListAdapter()).isOpen(tree) ? "collapseTree" : "expandTree").getValue());
            menu.add(0, READ_BOOK_ITEM_ID, 0, resource.getResource("readText").getValue());
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        final int position = ((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).position;
        final TOCTree tree = (TOCTree) myAdapter.getItem(position);
        switch (item.getItemId()) {
            case PROCESS_TREE_ITEM_ID:
                myAdapter.runTreeItem(tree);
                return true;
            case READ_BOOK_ITEM_ID:
                myAdapter.openBookText(tree);
                return true;
        }
        return super.onContextItemSelected(item);
    }

    private final class TOCAdapter extends ZLTreeAdapter {
        TOCAdapter(ListView list, TOCTree root) {
            super(list, root);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final View view = (convertView != null) ? convertView :
                    LayoutInflater.from(parent.getContext()).inflate(R.layout.toc_tree_item, parent, false);
            final TOCTree tree = (TOCTree) getItem(position);
            view.setBackgroundColor(tree == mySelectedItem ? 0xff808080 : 0);
            setIcon(ViewUtil.findImageView(view, R.id.toc_tree_item_icon), tree);
            ViewUtil.findTextView(view, R.id.toc_tree_item_text).setText(tree.getText() + " [" + tree.getReference().ParagraphIndex + "]");
            return view;
        }

        void openBookText(TOCTree tree) {
            final TOCTree.Reference reference = tree.getReference();
            if (reference != null) {
                getActivity().finish();
                final FBReaderApp fbreader = (FBReaderApp) ZLApplication.Instance();
                fbreader.addInvisibleBookmark();
                fbreader.BookTextView.gotoPosition(reference.ParagraphIndex, 0, 0);
                fbreader.showBookTextView();
                fbreader.storePosition();
            }
        }

        @Override
        protected boolean runTreeItem(ZLTree<?> tree) {
            if (super.runTreeItem(tree)) {
                return true;
            }
            openBookText((TOCTree) tree);
            return true;
        }
    }
}
