package com.somobu.smolreader.bookmarks;

import android.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.somobu.smolreader.R;

public class BookmarksContainerFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.v2_fragment_bookmarks, container, false);

        Fragment[] fragments = {
                new TOCFragment(),
                BookmarksFragment.instance(true),
                BookmarksFragment.instance(false)
        };

        String[] titles = {
                "TOC",
                "This book",
                "All books"
        };

        ViewPager pager = root.findViewById(R.id.pager);
        pager.setAdapter(new PagerAdapter() {

            final View[] views = new View[fragments.length];

            @Override
            public int getCount() {
                return fragments.length;
            }

            @Override
            public Object instantiateItem(ViewGroup container, int position) {
                getFragmentManager().beginTransaction().add(container.getId(), fragments[position]).commit();
                getFragmentManager().executePendingTransactions();
                views[position] = container.getChildAt(container.getChildCount() - 1);
                return fragments[position];
            }

            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
                getFragmentManager().beginTransaction().remove(fragments[position]).commit();
                getFragmentManager().executePendingTransactions();
                views[position] = null;
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                int frIdx = -1;

                for (int i = 0; i < fragments.length; i++) {
                    if (fragments[i] == object) frIdx = i;
                }

                if (frIdx == -1) return false;
                else return view == views[frIdx];
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return titles[position];
            }
        });


        TabLayout tl = root.findViewById(R.id.tabs);
        tl.setupWithViewPager(pager);

        return root;
    }


}
