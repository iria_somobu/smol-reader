package com.somobu.smolreader.bookmarks;

import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.somobu.smolreader.R;
import com.somobu.smolreader.UglyStore;

import org.geometerplus.android.fbreader.FBReader;
import org.geometerplus.android.fbreader.FBReaderIntents;
import org.geometerplus.android.fbreader.OrientationUtil;
import org.geometerplus.android.fbreader.bookmark.BookmarksUtil;
import org.geometerplus.android.fbreader.bookmark.EditBookmarkActivity;
import org.geometerplus.android.util.UIMessageUtil;
import org.geometerplus.android.util.ViewUtil;
import org.geometerplus.fbreader.book.Book;
import org.geometerplus.fbreader.book.Bookmark;
import org.geometerplus.fbreader.book.BookmarkQuery;
import org.geometerplus.fbreader.book.HighlightingStyle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import yuku.ambilwarna.widget.AmbilWarnaPrefWidgetView;

public class BookmarksFragment extends ListFragment {

    private static final int OPEN_ITEM_ID = 0;
    private static final int EDIT_ITEM_ID = 1;
    private static final int DELETE_ITEM_ID = 2;

    static BookmarksFragment instance(boolean showOnlyCurrentBook) {
        BookmarksFragment fragment = new BookmarksFragment();

        Bundle arguments = new Bundle();
        arguments.putBoolean("onlyCurrent", showOnlyCurrentBook);
        fragment.setArguments(arguments);

        return fragment;
    }

    private boolean isOnlyCurrent() {
        return getArguments().getBoolean("onlyCurrent");
    }

    private final Comparator<Bookmark> myComparator = new Bookmark.ByTimeComparator();
    private final Map<Integer, HighlightingStyle> myStyles = Collections.synchronizedMap(new HashMap<>());

    private volatile Book myBook;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        registerForContextMenu(getListView());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        setListAdapter(new BookmarksAdapter());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.v2_listfragment, container, false);
        ((TextView) root.findViewById(android.R.id.empty)).setText("Bookmarks list is empty");
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();

        UglyStore.local(getActivity(), store -> {
            myBook = FBReaderIntents.getBookExtra(getActivity().getIntent(), store);

            synchronized (myStyles) {
                myStyles.clear();
                for (HighlightingStyle style : store.highlightingStyles()) {
                    myStyles.put(style.Id, style);
                }
            }

            ArrayList<Bookmark> bookmarks = new ArrayList<>();
            for (BookmarkQuery query = new BookmarkQuery(myBook, 50); ; query = query.next()) {
                final List<Bookmark> thisBookBookmarks = store.bookmarks(query);
                if (thisBookBookmarks.isEmpty()) {
                    break;
                }
                bookmarks.addAll(thisBookBookmarks);
            }

            if (!isOnlyCurrent()) {
                for (BookmarkQuery query = new BookmarkQuery(50); ; query = query.next()) {
                    final List<Bookmark> allBookmarks = store.bookmarks(query);
                    if (allBookmarks.isEmpty()) {
                        break;
                    }
                    bookmarks.addAll(allBookmarks);
                }
            }

            BookmarksAdapter adapter = (BookmarksAdapter) getListAdapter();
            if(adapter == null){
                adapter = new BookmarksAdapter();
                setListAdapter(adapter);
            }
            adapter.replace(bookmarks);
        });
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        final Bookmark bookmark = ((BookmarksAdapter) getListAdapter()).getItem(position);
        if (bookmark != null) {
            gotoBookmark(bookmark);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) menuInfo;

        int position = acmi.position;
        if (getListAdapter().getItem(position) != null) {
            menu.add(position, OPEN_ITEM_ID, 0, "Open book");
            menu.add(position, EDIT_ITEM_ID, 0, "Edit bookmark");
            menu.add(position, DELETE_ITEM_ID, 0, "Delete bookmark");
        }
    }

    @Override
    public final boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        if (getListView().getPositionForView(info.targetView) == -1)
            return super.onContextItemSelected(item);

        int position = ((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).position;
        BookmarksAdapter adapter = (BookmarksAdapter) getListAdapter();
        Bookmark bookmark = adapter.getItem(position);
        switch (item.getItemId()) {
            case OPEN_ITEM_ID:
                gotoBookmark(bookmark);
                return true;
            case EDIT_ITEM_ID:
                final Intent intent = new Intent(getActivity(), EditBookmarkActivity.class);
                FBReaderIntents.putBookmarkExtra(intent, bookmark);
                OrientationUtil.startActivity(getActivity(), intent);
                return true;
            case DELETE_ITEM_ID:
                UglyStore.local(getActivity(), store -> store.deleteBookmark(bookmark));
                return true;
        }
        return super.onContextItemSelected(item);
    }

    private void gotoBookmark(Bookmark bookmark) {
        bookmark.markAsAccessed();
        UglyStore.local(getActivity(), myCollection -> {
            myCollection.saveBookmark(bookmark);
            final Book book = myCollection.getBookById(bookmark.BookId);
            if (book != null) {
                FBReader.openBookActivity(getActivity(), book, bookmark);
            } else {
                UIMessageUtil.showErrorMessage(getActivity(), "cannotOpenBook");
            }
        });

    }

    private final class BookmarksAdapter extends BaseAdapter {

        private final List<Bookmark> myBookmarksList = Collections.synchronizedList(new LinkedList<>());

        public List<Bookmark> bookmarks() {
            return Collections.unmodifiableList(myBookmarksList);
        }

        public void replace(final List<Bookmark> bookmarks) {
            myBookmarksList.clear();
            myBookmarksList.addAll(bookmarks);
            notifyDataSetChanged();
        }

        private boolean areEqualsForView(Bookmark b0, Bookmark b1) {
            return
                    b0.getStyleId() == b1.getStyleId() &&
                            b0.getText().equals(b1.getText()) &&
                            b0.getTimestamp(Bookmark.DateType.Latest).equals(b1.getTimestamp(Bookmark.DateType.Latest));
        }

        public void replace(final Bookmark old, final Bookmark b) {
            if (old != null && areEqualsForView(old, b)) {
                return;
            }

            synchronized (myBookmarksList) {
                if (old != null) {
                    myBookmarksList.remove(old);
                }
                final int position = Collections.binarySearch(myBookmarksList, b, myComparator);
                if (position < 0) {
                    myBookmarksList.add(-position - 1, b);
                }
            }
            notifyDataSetChanged();
        }

        public void clear() {
            myBookmarksList.clear();
            notifyDataSetChanged();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final View view = (convertView != null) ? convertView :
                    LayoutInflater.from(parent.getContext()).inflate(R.layout.bookmark_item, parent, false);
            final ImageView imageView = ViewUtil.findImageView(view, R.id.bookmark_item_icon);
            final View colorContainer = ViewUtil.findView(view, R.id.bookmark_item_color_container);
            final AmbilWarnaPrefWidgetView colorView =
                    (AmbilWarnaPrefWidgetView) ViewUtil.findView(view, R.id.bookmark_item_color);
            final TextView textView = ViewUtil.findTextView(view, R.id.bookmark_item_text);
            final TextView bookTitleView = ViewUtil.findTextView(view, R.id.bookmark_item_booktitle);

            final Bookmark bookmark = getItem(position);
            if (bookmark == null) {
                imageView.setVisibility(View.VISIBLE);
                imageView.setImageResource(R.drawable.ic_list_plus);
                colorContainer.setVisibility(View.GONE);
                textView.setText("New bookmark");
                bookTitleView.setVisibility(View.GONE);
            } else {
                imageView.setVisibility(View.GONE);
                colorContainer.setVisibility(View.VISIBLE);
                BookmarksUtil.setupColorView(colorView, myStyles.get(bookmark.getStyleId()));
                textView.setText(bookmark.getText());
                if (isOnlyCurrent()) {
                    bookTitleView.setVisibility(View.GONE);
                } else {
                    bookTitleView.setVisibility(View.VISIBLE);
                    bookTitleView.setText(bookmark.BookTitle);
                }
            }
            return view;
        }

        @Override
        public final long getItemId(int position) {
            final Bookmark item = getItem(position);
            return item != null ? item.getId() : -1;
        }

        @Override
        public final Bookmark getItem(int position) {
            return myBookmarksList.get(position);
        }

        @Override
        public final int getCount() {
            return myBookmarksList.size();
        }
    }
}
