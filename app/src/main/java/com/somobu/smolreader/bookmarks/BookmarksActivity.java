package com.somobu.smolreader.bookmarks;

import android.app.Activity;
import android.os.Bundle;

import com.somobu.smolreader.R;
import com.somobu.smolreader.Theming;

public class BookmarksActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Theming.onCreate(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.v2_activity_bookmarks);
    }

}
