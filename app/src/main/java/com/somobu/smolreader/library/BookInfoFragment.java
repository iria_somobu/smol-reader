package com.somobu.smolreader.library;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.method.LinkMovementMethod;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.somobu.smolreader.ISynchronizerProvider;
import com.somobu.smolreader.R;
import com.somobu.smolreader.UglyStore;

import org.geometerplus.android.fbreader.FBReader;
import org.geometerplus.android.fbreader.FBReaderIntents;
import org.geometerplus.android.fbreader.FBUtil;
import org.geometerplus.android.fbreader.OrientationUtil;
import org.geometerplus.android.fbreader.libraryService.BookCollectionShadow;
import org.geometerplus.android.fbreader.preferences.EditBookInfoActivity;
import org.geometerplus.android.fbreader.util.AndroidImageSynchronizer;
import org.geometerplus.fbreader.Paths;
import org.geometerplus.fbreader.book.Author;
import org.geometerplus.fbreader.book.Book;
import org.geometerplus.fbreader.book.BookUtil;
import org.geometerplus.fbreader.book.CoverUtil;
import org.geometerplus.fbreader.book.SeriesInfo;
import org.geometerplus.fbreader.book.Tag;
import org.geometerplus.fbreader.formats.PluginCollection;
import org.geometerplus.fbreader.network.HtmlUtil;
import org.geometerplus.fbreader.network.NetworkLibrary;
import org.geometerplus.zlibrary.core.filesystem.ZLFile;
import org.geometerplus.zlibrary.core.filesystem.ZLPhysicalFile;
import org.geometerplus.zlibrary.core.image.ZLImage;
import org.geometerplus.zlibrary.core.image.ZLImageProxy;
import org.geometerplus.zlibrary.core.language.Language;
import org.geometerplus.zlibrary.core.language.ZLLanguageUtil;
import org.geometerplus.zlibrary.image.ZLAndroidImageData;
import org.geometerplus.zlibrary.image.ZLAndroidImageManager;

import java.io.File;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

public class BookInfoFragment extends Fragment {

    private static final boolean ENABLE_EXTENDED_FILE_INFO = true;
    public static final String KEY_BOOK_FILE = "book_file";

    private View cachedRoot = null;

    public static BookInfoFragment instance(Book book) {
        return instance(book.getPath());
    }

    public static BookInfoFragment instance(String bookPath) {
        BookInfoFragment instance = new BookInfoFragment();

        Bundle args = new Bundle();
        args.putString(KEY_BOOK_FILE, bookPath);
        instance.setArguments(args);

        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.book_info, container, false);
        cachedRoot = root;

        // TODO: loader placeholder

        UglyStore.local(getActivity(), store -> {
            Book book = store.getBookByFile(getBookPath());

            if (book == null) {
                // TODO: proper error handling
                throw new RuntimeException("We've got null book somehow");
            }

            final PluginCollection pluginCollection = PluginCollection.Instance(Paths.systemInfo(getActivity()));

            // we force language & encoding detection
            BookUtil.getEncoding(book, pluginCollection);

            setupActions(store, book);
            setupCover(book, pluginCollection);
            setupBookInfo(book);
            setupAnnotation(book, pluginCollection);
            setupFileInfo(book);
        });

        return root;
    }

    private View findViewById(int id) {
        if (getView() != null) return getView().findViewById(id);
        else return cachedRoot.findViewById(id);
    }

    private String getBookPath() {
        return getArguments().getString(KEY_BOOK_FILE);
    }

    private void setupActions(BookCollectionShadow store, Book book) {
        findViewById(R.id.btn_open).setOnClickListener(view ->
                FBReader.openBookActivity(getActivity(), book, null));

        findViewById(R.id.btn_edit).setOnClickListener(view -> {
            final Intent intent = new Intent(getActivity(), EditBookInfoActivity.class);
            FBReaderIntents.putBookExtra(intent, book);
            OrientationUtil.startActivity(getActivity(), intent);
        });

        findViewById(R.id.btn_share).setOnClickListener(view ->
                FBUtil.shareBook(getActivity(), book));


        findViewById(R.id.btn_fav).setOnClickListener(view -> {
            if (book.hasLabel(Book.FAVORITE_LABEL)) book.removeLabel(Book.FAVORITE_LABEL);
            else book.addNewLabel(Book.FAVORITE_LABEL);
            store.saveBook(book);

            String lbl = book.hasLabel(Book.FAVORITE_LABEL) ? "Marked as favorite" : "Favorite mark removed";
            Toast.makeText(getActivity(), lbl, Toast.LENGTH_SHORT).show();
        });

        findViewById(R.id.btn_mark_read).setOnClickListener(view -> {
            if (book.hasLabel(Book.READ_LABEL)) book.removeLabel(Book.READ_LABEL);
            else book.addNewLabel(Book.READ_LABEL);
            store.saveBook(book);

            String lbl = book.hasLabel(Book.READ_LABEL) ? "Marked as read" : "Marked as unread";
            Toast.makeText(getActivity(), lbl, Toast.LENGTH_SHORT).show();
        });
    }

    private void setupInfoPair(int id, String key, CharSequence value) {
        final LinearLayout layout = (LinearLayout) findViewById(id);
        if (value == null || value.length() == 0) {
            layout.setVisibility(View.GONE);
            return;
        }
        layout.setVisibility(View.VISIBLE);
        ((TextView) layout.findViewById(R.id.book_info_key)).setText(key);
        ((TextView) layout.findViewById(R.id.book_info_value)).setText(value);
    }

    private void setupCover(Book book, PluginCollection pluginCollection) {
        final ImageView coverView = (ImageView) findViewById(R.id.book_cover);

        coverView.setVisibility(View.GONE);
        coverView.setImageDrawable(null);

        final ZLImage image = CoverUtil.getCover(book, pluginCollection);
        if (image == null) return;

        if (image instanceof ZLImageProxy) {
            Handler h = new Handler();
            AndroidImageSynchronizer ais = ((ISynchronizerProvider) getActivity()).getImageSynchronizer();
            ((ZLImageProxy) image).startSynchronization(ais, () -> h.post(() -> setCover(coverView, image)));
        } else {
            setCover(coverView, image);
        }
    }

    private void setCover(ImageView coverView, ZLImage image) {
        final ZLAndroidImageData data = ((ZLAndroidImageManager) ZLAndroidImageManager.Instance()).getImageData(image);
        if (data == null) return;

        final DisplayMetrics metrics = new DisplayMetrics();
        Activity activity = getActivity();
        if(activity == null) return;
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        final int maxHeight = metrics.heightPixels * 2 / 3;
        final int maxWidth = maxHeight * 2 / 3;

        final Bitmap coverBitmap = data.getBitmap(2 * maxWidth, 2 * maxHeight);
        if (coverBitmap == null) return;

        coverView.setVisibility(View.VISIBLE);
        coverView.getLayoutParams().width = maxWidth;
        coverView.getLayoutParams().height = maxHeight;
        coverView.setImageBitmap(coverBitmap);
    }

    private void setupBookInfo(Book book) {
        setupInfoPair(R.id.book_title, "Title:", book.getTitle());

        final StringBuilder buffer = new StringBuilder();
        final List<Author> authors = book.authors();
        for (Author a : authors) {
            if (buffer.length() > 0) buffer.append(", ");
            buffer.append(a.DisplayName);
        }
        setupInfoPair(R.id.book_authors, "Authors:", buffer);

        final SeriesInfo series = book.getSeriesInfo();
        setupInfoPair(R.id.book_series, "Series:", series == null ? null : series.Series.getTitle());
        String seriesIndexString = null;
        if (series != null && series.Index != null) {
            seriesIndexString = series.Index.toPlainString();
        }
        setupInfoPair(R.id.book_series_index, "Number in series:", seriesIndexString);

        buffer.delete(0, buffer.length());
        final HashSet<String> tagNames = new HashSet<String>();
        for (Tag tag : book.tags()) {
            if (!tagNames.contains(tag.Name)) {
                if (buffer.length() > 0) buffer.append(", ");
                buffer.append(tag.Name);
                tagNames.add(tag.Name);
            }
        }
        setupInfoPair(R.id.book_tags, "Tags:", buffer);
        String language = book.getLanguage();
        if (!ZLLanguageUtil.languageCodes().contains(language)) {
            language = Language.OTHER_CODE;
        }
        setupInfoPair(R.id.book_language, "Language:", new Language(language).Name);
    }

    private void setupAnnotation(Book book, PluginCollection pluginCollection) {
        final TextView titleView = (TextView) findViewById(R.id.book_info_annotation_title);
        final TextView bodyView = (TextView) findViewById(R.id.book_info_annotation_body);
        final String annotation = BookUtil.getAnnotation(book, pluginCollection);
        if (annotation == null) {
            titleView.setVisibility(View.GONE);
            bodyView.setVisibility(View.GONE);
        } else {
            bodyView.setText(HtmlUtil.getHtmlText(NetworkLibrary.Instance(Paths.systemInfo(getActivity())), annotation));
            bodyView.setMovementMethod(new LinkMovementMethod());
            bodyView.setTextColor(ColorStateList.valueOf(bodyView.getTextColors().getDefaultColor()));
        }
    }

    private void setupFileInfo(Book book) {
        setupInfoPair(R.id.file_name, "Name:", book.getPath());
        if (ENABLE_EXTENDED_FILE_INFO) {
            final ZLFile bookFile = BookUtil.fileByBook(book);
            setupInfoPair(R.id.file_type, "Type:", bookFile.getExtension());

            final ZLPhysicalFile physFile = bookFile.getPhysicalFile();
            final File file = physFile == null ? null : physFile.javaFile();
            if (file != null && file.exists() && file.isFile()) {
                setupInfoPair(R.id.file_size, "Size:", formatSize(file.length()));
                setupInfoPair(R.id.file_time, "Time:", formatDate(file.lastModified()));
            } else {
                setupInfoPair(R.id.file_size, "Size:", null);
                setupInfoPair(R.id.file_time, "Time:", null);
            }
        } else {
            setupInfoPair(R.id.file_type, "Type:", null);
            setupInfoPair(R.id.file_size, "Size:", null);
            setupInfoPair(R.id.file_time, "Time:", null);
        }
    }

    private String formatSize(long size) {
        if (size <= 0) return null;
        final int kilo = 1024;
        if (size < kilo) { // less than 1 kilobyte
            return String.format("%s bytes", size);
        }

        final String value;
        if (size < kilo * kilo) { // less than 1 megabyte
            value = String.format("%.2f", ((float) size) / kilo);
        } else {
            value = String.valueOf(size / kilo);
        }
        return String.format("%s kB", value);
    }

    private String formatDate(long date) {
        if (date == 0) return null;
        return DateFormat.getDateTimeInstance().format(new Date(date));
    }

}
