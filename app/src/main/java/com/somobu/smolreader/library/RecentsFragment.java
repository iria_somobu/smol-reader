package com.somobu.smolreader.library;

import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.somobu.smolreader.ISynchronizerProvider;
import com.somobu.smolreader.UglyStore;

import org.geometerplus.android.fbreader.FBReader;
import org.geometerplus.fbreader.book.Book;

import java.util.List;

public class RecentsFragment extends AbstractLibFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = super.onCreateView(inflater, container, savedInstanceState);
        ((TextView) root.findViewById(android.R.id.empty)).setText("Recents list is empty");
        return root;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ISynchronizerProvider provider = (ISynchronizerProvider) getActivity();
        BookAdapter adapter = new BookAdapter(getActivity(), provider.getImageSynchronizer());
        setListAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();

        reload();
    }

    void reload() {
        UglyStore.local(getActivity(), store -> {
            List<Book> books = store.recentlyOpenedBooks(20);
            ((BookAdapter) getListAdapter()).replace(books);
        });
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Book item = ((BookAdapter) getListAdapter()).getItem(position);
        Intent intent = new Intent(getActivity(), BookInfoActivity.class);
        intent.putExtra(BookInfoFragment.KEY_BOOK_FILE, item.getPath());
        startActivity(intent);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == android.R.id.list) {
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) menuInfo;
            Book book = (Book) ((ListView) v).getItemAtPosition(acmi.position);
            menu.setHeaderTitle(book.getTitle());
            menu.add(acmi.position, 0, 0, "Open book in reader");
            menu.add(acmi.position, 1, 1, book.hasLabel(Book.FAVORITE_LABEL) ? "Remove from favorites" : "Add to favorites");
            menu.add(acmi.position, 2, 2, book.hasLabel(Book.READ_LABEL) ? "Mark as unread" : "Mark as read");
        }
    }

    @Override
    public void onContextListItemSelected(MenuItem item) {

        Book book = (Book) getListView().getItemAtPosition(item.getGroupId());

        switch (item.getItemId()) {
            case 0:
                FBReader.openBookActivity(getActivity(), book, null);
                break;
            case 1:
                if (book.hasLabel(Book.FAVORITE_LABEL)) book.removeLabel(Book.FAVORITE_LABEL);
                else book.addNewLabel(Book.FAVORITE_LABEL);
                break;
            case 2:
                if (book.hasLabel(Book.READ_LABEL)) book.removeLabel(Book.READ_LABEL);
                else book.addNewLabel(Book.READ_LABEL);
                break;
        }

        UglyStore.local(getActivity(), store -> {
            store.saveBook(book);
            ((ILibraryHost) getActivity()).notifyLibReload();
            reload();
        });
    }

    @Override
    public String getTitle() {
        return "Recent books";
    }

    @Override
    public String getSubtitle() {
        return null;
    }

    @Override
    public Action getActionIcon() {
        return null;
    }
}
