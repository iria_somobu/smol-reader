package com.somobu.smolreader.library;

public interface ILibraryHost {

    void setSubtitle(String subtitle);

    void notifyLibReload();

}
