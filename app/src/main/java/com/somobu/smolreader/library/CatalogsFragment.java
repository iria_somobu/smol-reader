package com.somobu.smolreader.library;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Html;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.somobu.smolreader.ISynchronizerProvider;
import com.somobu.smolreader.R;
import com.somobu.smolreader.UglyStore;
import com.somobu.smolreader.prefs.PreferencesActivity;

import org.geometerplus.android.fbreader.FBReader;
import org.geometerplus.android.fbreader.covers.CoverManager;
import org.geometerplus.android.fbreader.util.AndroidImageSynchronizer;
import org.geometerplus.android.util.ViewUtil;
import org.geometerplus.fbreader.Paths;
import org.geometerplus.fbreader.book.Author;
import org.geometerplus.fbreader.book.Book;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class CatalogsFragment extends AbstractLibFragment {

    private final Action action;
    private File currentFolder = null;

    public CatalogsFragment() {
        action = new Action(R.drawable.ic_menu_library, "Manage catalogs",
                () -> {
                    Intent i = new Intent(getActivity(), PreferencesActivity.class);
                    startActivity(i);
                });
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((TextView) view.findViewById(android.R.id.empty)).setText("No books found\n" +
                "You can setup libraries folders from the settings");

        ISynchronizerProvider provider = (ISynchronizerProvider) getActivity();
        setListAdapter(new FileListAdapter(getActivity(), provider.getImageSynchronizer()));
        updateAdapter(currentFolder);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Object item = ((FileListAdapter) getListAdapter()).getItem(position);

        if (item instanceof Book) {
            Intent intent = new Intent(getActivity(), BookInfoActivity.class);
            intent.putExtra(BookInfoFragment.KEY_BOOK_FILE, ((Book) item).getPath());
            startActivity(intent);
        } else if (item instanceof File) {
            File file = (File) item;
            if (file.isDirectory()) {
                ((ILibraryHost) getActivity()).setSubtitle(describeSubtitle(file));
                updateAdapter(file);
            } else {
                Toast.makeText(getActivity(), "Unsupported!", Toast.LENGTH_SHORT).show();
            }
        } else {
            File cf = currentFolder == null ? currentFolder : currentFolder.getParentFile();
            ((ILibraryHost) getActivity()).setSubtitle(describeSubtitle(cf));
            updateAdapter(cf);
        }
    }

    private void updateAdapter(File directory) {
        final File[] files;

        if (shouldListLibrariesInstead(directory)) {
            List<String> dirs = Paths.getBooksPathList(getActivity());

            if (dirs.size() == 1) {
                File[] df = new File(dirs.get(0)).listFiles();
                if (df == null) {
                    files = new File[0];
                } else {
                    files = df;
                }
            } else {
                files = new File[dirs.size()];

                for (int i = 0; i < dirs.size(); i++) {
                    files[i] = new File(dirs.get(i));
                }
            }

        } else {
            File[] thisfiles = directory.listFiles();
            files = new File[thisfiles.length + 1];
            files[files.length - 1] = null;
            System.arraycopy(thisfiles, 0, files, 0, thisfiles.length);
        }

        Arrays.sort(files, new Comparator<File>() {

            @Override
            public int compare(File file, File t1) {
                if (file == null) return -1;
                if (t1 == null) return 1;
                if (file.isDirectory() && !t1.isDirectory()) return -1;
                if (!file.isDirectory() && t1.isDirectory()) return 1;
                return file.compareTo(t1);
            }
        });

        UglyStore.local(getActivity(), store -> {
            currentFolder = directory;

            List<Object> hybrid = new ArrayList<>();

            for (File file : files) {
                if (file == null || file.isDirectory()) {
                    hybrid.add(file);
                } else {
                    Book book = store.getBookByFile(file.getAbsolutePath());
                    if (book != null) {
                        // TODO: null-book may be an archive
                        hybrid.add(book);
                    }
                }
            }

            boolean sn = shouldDisplayFolderPathInAdapter(directory);
            ((FileListAdapter) getListAdapter()).replace(sn, hybrid.toArray(new Object[0]));
        });
    }

    private boolean shouldDisplayFolderPathInAdapter(File directory) {

        List<String> paths = Paths.getBooksPathList(getActivity());
        if (paths.size() == 1) {
            return false;
        } else {
            if (directory == null) return true;
            for (String s : paths) {
                if (directory.getAbsolutePath().contains(s)) return false;
            }
        }

        return true;
    }

    private boolean shouldListLibrariesInstead(File directory) {
        if (directory == null) return true;

        List<String> paths = Paths.getBooksPathList(getActivity());
        if (paths.size() == 1) {
            String dir = paths.get(0);
            if (directory.getAbsolutePath().equals(dir)) return true;
            if (directory.getAbsolutePath().contains(dir)) return false;
            return true;
        } else {
            for (String s : paths) {
                if (directory.getAbsolutePath().contains(s)) return false;
            }

            return true;
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == android.R.id.list) {
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) menuInfo;
            Object item = ((ListView) v).getItemAtPosition(acmi.position);
            if (item instanceof Book) {
                Book book = (Book) item;
                menu.setHeaderTitle(book.getTitle());
                menu.add(acmi.position, 0, 0, "Open book in reader");
                menu.add(acmi.position, 1, 1, book.hasLabel(Book.FAVORITE_LABEL) ? "Remove from favorites" : "Add to favorites");
                menu.add(acmi.position, 2, 2, book.hasLabel(Book.READ_LABEL) ? "Mark as unread" : "Mark as read");
            }
        }
    }

    @Override
    public void onContextListItemSelected(MenuItem item) {

        Book book = (Book) getListView().getItemAtPosition(item.getGroupId());

        switch (item.getItemId()) {
            case 0:
                FBReader.openBookActivity(getActivity(), book, null);
                break;
            case 1:
                if (book.hasLabel(Book.FAVORITE_LABEL)) book.removeLabel(Book.FAVORITE_LABEL);
                else book.addNewLabel(Book.FAVORITE_LABEL);
                break;
            case 2:
                if (book.hasLabel(Book.READ_LABEL)) book.removeLabel(Book.READ_LABEL);
                else book.addNewLabel(Book.READ_LABEL);
                break;
        }

        UglyStore.local(getActivity(), store -> {
            store.saveBook(book);
            ((ILibraryHost) getActivity()).notifyLibReload();
            updateAdapter(currentFolder);
        });
    }

    @Override
    public String getTitle() {
        return "Library";
    }

    @Override
    public String getSubtitle() {
        return describeSubtitle(currentFolder);
    }

    @Override
    public Action getActionIcon() {
        return action;
    }

    private String describeSubtitle(File currentFolder) {
        // shouldListLibrariesInstead needs not-null activity to work with
        boolean listLib = getActivity() == null || shouldListLibrariesInstead(currentFolder);

        return currentFolder == null || listLib
                ? "Library root"
                : currentFolder.getAbsolutePath();
    }

    private static class FileListAdapter extends ArrayAdapter<Object> {

        private final List<Object> books = new ArrayList<>();
        private final AndroidImageSynchronizer ImageSynchronizer;
        private CoverManager myCoverManager;
        private boolean shouldDescribeFiles = false;

        public FileListAdapter(@NonNull Context context, AndroidImageSynchronizer sync) {
            super(context, R.layout.library_tree_item);
            this.ImageSynchronizer = sync;
        }

        public void replace(boolean shouldDescribeFiles, Object[] replacement) {
            this.books.clear();
            this.shouldDescribeFiles = shouldDescribeFiles;
            books.addAll(Arrays.asList(replacement));
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return books.size();
        }

        @Override
        public Object getItem(int i) {
            return books.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        public String getSummary(Book book) {
            StringBuilder builder = new StringBuilder();
            int count = 0;
            for (Author author : book.authors()) {
                if (count++ > 0) {
                    builder.append(",  ");
                }
                builder.append(author.DisplayName);
                if (count == 5) {
                    break;
                }
            }
            return builder.toString();
        }

        @SuppressLint("ViewHolder")
        @Override
        public View getView(int position, View convertView, final ViewGroup parent) {
            /*
            TODO: here we're inflating view unconditionally cuz we cannot cancel image loading
            on given convert view. CoverManager has to be changed, but I have no time for this rn.
             */
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.library_tree_item, parent, false);

            if (myCoverManager == null) {
                convertView.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
                myCoverManager = new CoverManager(getContext(), ImageSynchronizer,
                        convertView.getMeasuredWidth(),
                        convertView.getMeasuredHeight());
                myCoverManager.workaroundDisableParamsOverride = true;
                convertView.requestLayout();
            }

            Object item = getItem(position);

            final ImageView coverView = ViewUtil.findImageView(convertView, R.id.library_tree_item_icon);
            final TextView nameView = ViewUtil.findTextView(convertView, R.id.library_tree_item_name);
            final TextView summaryView = ViewUtil.findTextView(convertView, R.id.library_tree_item_childrenlist);

            if (item instanceof Book) {
                Book book = (Book) item;

                final boolean unread = !book.hasLabel(Book.READ_LABEL);
                nameView.setText(unread ? Html.fromHtml("<b>" + book.getTitle()) : book.getTitle());
                summaryView.setText(unread ? Html.fromHtml("<b>" + getSummary(book)) : getSummary(book));

                if (!myCoverManager.trySetCoverImage(coverView, (Book) item)) {
                    coverView.setImageResource(R.drawable.ic_list_library_book);
                }

            } else {
                if (item instanceof File) {
                    File file = (File) item;
                    nameView.setText(file.getName());
                    summaryView.setText(shouldDescribeFiles ? file.getParent() : "");
                    if (file.isDirectory()) {
                        coverView.setImageResource(R.drawable.ic_list_library_folder);
                    } else {
                        coverView.setImageResource(R.drawable.ic_menu_none);
                    }
                } else {
                    nameView.setText("..");
                    summaryView.setText("Go up");
                    coverView.setImageResource(0);
                }

            }

            convertView.setBackgroundColor(0);
            summaryView.setVisibility(summaryView.getText().toString().isEmpty() ? View.GONE : View.VISIBLE);

            return convertView;
        }
    }
}
