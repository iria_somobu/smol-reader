package com.somobu.smolreader.library;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.somobu.smolreader.R;

import org.geometerplus.android.util.ViewUtil;

public class SearchFragment extends AbstractLibFragment {

    private static class Entry {
        String title, descr;
        int icon;
        Intent action;

        public Entry(String title, String descr, int icon) {
            this.title = title;
            this.descr = descr;
            this.icon = icon;
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Entry[] entries = {
                new Entry("Local by name", "Search books in all local catalogs by title", R.drawable.ic_list_library_search),
                new Entry("Network by name", "Search books in all network catalogs by title", R.drawable.ic_list_library_search),
                new Entry("By author", "Books sorted by author", R.drawable.ic_list_library_authors),
                new Entry("By title", "Books sorted by title", R.drawable.ic_list_library_books),
                new Entry("By series", "Books sorted by series", R.drawable.ic_list_library_books),
                new Entry("By tag", "Books sorted by tag", R.drawable.ic_list_library_tags)
        };

        setListAdapter(new ArrayAdapter<Entry>(getActivity(), android.R.layout.simple_list_item_1, entries) {

            @Override
            public View getView(int position, View convertView, final ViewGroup parent) {
                if (convertView == null) {
                    convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.library_tree_item, parent, false);
                }

                Entry e = getItem(position);
                ViewUtil.findTextView(convertView, R.id.library_tree_item_name).setText(e.title);
                ViewUtil.findTextView(convertView, R.id.library_tree_item_childrenlist).setText(e.descr);
                ViewUtil.findImageView(convertView, R.id.library_tree_item_icon).setImageResource(e.icon);

                convertView.setBackgroundColor(0);
                return convertView;
            }

        });
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        switch (position) {
            case 0: // Search (local)
            case 1: // Search (network)
            case 2: // Filter by author
            case 3: // Filter by title
            case 4: // Filter by series
            case 5: // Filter by tag
                Toast.makeText(getActivity(), "Unimplemented yet", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public String getTitle() {
        return "Search";
    }

    @Override
    public String getSubtitle() {
        return null;
    }

    @Override
    public Action getActionIcon() {
        return null;
    }

    private void search() {

    }
}
