package com.somobu.smolreader.library;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.somobu.smolreader.ISynchronizerProvider;
import com.somobu.smolreader.R;
import com.somobu.smolreader.UglyStore;

import org.geometerplus.android.fbreader.covers.CoverManager;
import org.geometerplus.android.fbreader.network.AddCustomCatalogActivity;
import org.geometerplus.android.fbreader.network.NetworkBookInfoActivity;
import org.geometerplus.android.fbreader.network.Util;
import org.geometerplus.android.fbreader.network.auth.ActivityNetworkContext;
import org.geometerplus.android.fbreader.util.AndroidImageSynchronizer;
import org.geometerplus.android.util.UIUtil;
import org.geometerplus.android.util.ViewUtil;
import org.geometerplus.fbreader.network.ICustomNetworkLink;
import org.geometerplus.fbreader.network.NetworkBookItem;
import org.geometerplus.fbreader.network.NetworkLibrary;
import org.geometerplus.fbreader.network.NetworkTree;
import org.geometerplus.fbreader.network.tree.CatalogExpander;
import org.geometerplus.fbreader.network.tree.NetworkBookTree;
import org.geometerplus.fbreader.network.tree.NetworkCatalogRootTree;
import org.geometerplus.fbreader.network.tree.NetworkCatalogTree;
import org.geometerplus.fbreader.network.tree.SearchCatalogTree;
import org.geometerplus.fbreader.tree.FBTree;
import org.geometerplus.zlibrary.network.SQLiteCookieDatabase;

import java.util.ArrayList;
import java.util.List;

public class NetworkFragment extends AbstractLibFragment {

    private static final int RQ_RELOAD = 4221;

    private final Action action;
    private ActivityNetworkContext myNetworkContext = null;
    private NetworkCatalogTree currentItem = null;

    private boolean invalidateCatalogsOnResume = false;

    public NetworkFragment() {
        action = new Action(R.drawable.ic_list_plus, "Add catalog",
                () -> {
                    invalidateCatalogsOnResume = true;
                    Intent i = new Intent(getActivity(), AddCustomCatalogActivity.class);
                    startActivityForResult(i, RQ_RELOAD);
                });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        SQLiteCookieDatabase.init(context);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setListAdapter(new HybridAdapter(getActivity(), ((ISynchronizerProvider) getActivity()).getImageSynchronizer()));

        myNetworkContext = new ActivityNetworkContext(getActivity());
        showRoot();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == RQ_RELOAD) {
            NetworkLibrary netlib = Util.networkLibrary(getActivity());
            netlib.synchronize();
            showRoot();
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (invalidateCatalogsOnResume) {
            invalidateCatalogsOnResume = false;
            Util.networkLibrary(getActivity())
                    .fireModelChangedEvent(NetworkLibrary.ChangeListener.Code.SomeCode);
            UglyStore.markReload();
            showRoot();
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        Object o = getListAdapter().getItem(position);

        if (o instanceof GoUpEntry) {
            if (currentItem == null) {
                showRoot();
                ((ILibraryHost) getActivity()).setSubtitle(describeSubtitle(null));
            } else if (currentItem.Parent == null) {
                showRoot();
                ((ILibraryHost) getActivity()).setSubtitle(describeSubtitle(null));
            } else if (currentItem.Parent instanceof NetworkCatalogTree) {
                showCatalog((NetworkCatalogTree) currentItem.Parent);
                ((ILibraryHost) getActivity()).setSubtitle(describeSubtitle(currentItem.Parent));
            } else {
                showRoot();
                ((ILibraryHost) getActivity()).setSubtitle(describeSubtitle(null));
            }
        } else if (o instanceof NetworkBookTree) {
            showBookInfo((NetworkBookTree) o);
        } else if (o instanceof NetworkCatalogTree) {
            showCatalog((NetworkCatalogTree) o);
            ((ILibraryHost) getActivity()).setSubtitle(describeSubtitle((NetworkCatalogTree) o));
        }
    }

    private void showBookInfo(NetworkTree tree) {
        if (((NetworkBookTree) tree).Book.isFullyLoaded()) {
            launchDatIntent(tree);
        } else {
            UIUtil.wait("loadInfo", () -> {
                ((NetworkBookTree) tree).Book.loadFullInformation(myNetworkContext);
                getActivity().runOnUiThread(() -> launchDatIntent(tree));
            }, getActivity());
        }
    }

    private void launchDatIntent(NetworkTree tree) {
        Intent bookInfoIntent = new Intent(getActivity(), NetworkBookInfoActivity.class);
        bookInfoIntent.putExtra(NetworkBookInfoActivity.TREE_KEY_KEY, tree.getUniqueKey());
        startActivityForResult(bookInfoIntent, 1);
    }

    private void showRoot() {
        ((HybridAdapter) getListAdapter()).set(new ArrayList<>());
        UglyStore.network(getActivity(), myNetworkContext, library -> {
            currentItem = null;
            ArrayList<NetworkCatalogRootTree> items = new ArrayList<>();
            for (FBTree tree : library.getRootTree()) {
                if (tree instanceof NetworkCatalogRootTree) {
                    items.add((NetworkCatalogRootTree) tree);
                }
            }
            ((HybridAdapter) getListAdapter()).set(items);
        });
    }

    private void showCatalog(NetworkCatalogTree parent) {
        Handler handler = new Handler();
        ((HybridAdapter) getListAdapter()).set(new ArrayList<>());
        UglyStore.network(getActivity(), myNetworkContext, library -> {

            boolean resumeNotLoad = false;
            if (parent.hasChildren()) {
                if (parent.isContentValid()) {
                    if (parent.Item.supportsResumeLoading()) {
                        resumeNotLoad = true;
                    }
                } else {
                    parent.clearCatalog();
                }
            }

            CatalogExpander ce = new CatalogExpander(myNetworkContext, parent, false, resumeNotLoad);
            ce.setPostRunnable(() -> handler.post(() -> {
                parent.waitForOpening();

                currentItem = parent;

                ArrayList<Object> items = new ArrayList<>();
                items.add(new GoUpEntry());
                for (Object o : parent.subtrees()) {
                    if (o instanceof SearchCatalogTree) continue;

                    items.add(o);
                }
                ((HybridAdapter) getListAdapter()).set(items);
            }));
            ce.start();
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == android.R.id.list) {
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) menuInfo;
            Object item = ((ListView) v).getItemAtPosition(acmi.position);
            if (item instanceof NetworkCatalogTree) {
                menu.add(acmi.position, 0, 0, "Edit catalog");
                menu.add(acmi.position, 1, 1, "Remove catalog");
            } else if (item instanceof NetworkBookTree) {
                menu.add(acmi.position, 2, 2, "Download book");
            }
        }
    }

    @Override
    public void onContextListItemSelected(MenuItem item) {
        Object obj = getListView().getItemAtPosition(item.getGroupId());
        switch (item.getItemId()) {
            case 0:
                final Intent intent = new Intent(getActivity(), AddCustomCatalogActivity.class);
                Util.intentByLink(intent, ((NetworkCatalogTree) obj).getLink());
                intent.setAction(Util.EDIT_CATALOG_ACTION);
                getActivity().startActivityForResult(intent, RQ_RELOAD);
                break;
            case 1:
                NetworkLibrary netlib = Util.networkLibrary(getActivity());
                netlib.removeCustomLink((ICustomNetworkLink) ((NetworkCatalogTree) obj).getLink());
                netlib.synchronize();
                showRoot();
                break;
            case 2:
                NetworkBookItem bookRef = ((NetworkBookTree) obj).Book;
                Util.doDownloadBook(getActivity(), bookRef, false);
                break;
        }
    }

    @Override
    public String getTitle() {
        return "Network library";
    }

    @Override
    public String getSubtitle() {
        return describeSubtitle(currentItem);
    }

    @Override
    public Action getActionIcon() {
        return action;
    }

    private String describeSubtitle(FBTree currentFolder) {
        if (currentFolder == null) {
            return "Library root";
        } else if (currentFolder instanceof NetworkCatalogRootTree) {
            return currentFolder.getName();
        } else {
            return describeSubtitle(currentFolder.Parent) + " / " + currentFolder.getName();
        }
    }

    static class GoUpEntry {
    }

    static class HybridAdapter extends ArrayAdapter<Object> {

        private CoverManager myCoverManager;
        private final AndroidImageSynchronizer ImageSynchronizer;

        public HybridAdapter(@NonNull Context context, AndroidImageSynchronizer sync) {
            super(context, R.layout.library_tree_item);
            this.ImageSynchronizer = sync;
        }

        public void set(List<?> o) {
            clear();
            addAll(o);
            notifyDataSetChanged();
        }

        @Override
        public View getView(int position, View convertView, final ViewGroup parent) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.library_tree_item, parent, false);

            if (myCoverManager == null) {
                convertView.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
                myCoverManager = new CoverManager(getContext(), ImageSynchronizer,
                        convertView.getMeasuredWidth(),
                        convertView.getMeasuredHeight());
                myCoverManager.workaroundDisableParamsOverride = true;
                convertView.requestLayout();
            }

            Object item = getItem(position);

            final ImageView coverView = ViewUtil.findImageView(convertView, R.id.library_tree_item_icon);
            final TextView nameView = ViewUtil.findTextView(convertView, R.id.library_tree_item_name);
            final TextView summaryView = ViewUtil.findTextView(convertView, R.id.library_tree_item_childrenlist);

            if (item instanceof GoUpEntry) {
                nameView.setText("..");
                summaryView.setText("Go up");
                coverView.setImageResource(0);
            } else if (item instanceof NetworkCatalogTree) {
                NetworkCatalogTree rt = (NetworkCatalogTree) item;
                nameView.setText(rt.getName());
                summaryView.setText(rt.getSummary());

                if (!myCoverManager.trySetCoverImage(coverView, (NetworkTree) item)) {
                    coverView.setImageResource(R.drawable.ic_list_library_books);
                }
            } else if (item instanceof NetworkBookTree) {
                NetworkBookTree nbt = (NetworkBookTree) item;

                nameView.setText(nbt.getName());
                summaryView.setText(nbt.getSummary());

                if (!myCoverManager.trySetCoverImage(coverView, nbt)) {
                    coverView.setImageResource(R.drawable.ic_list_library_book);
                }

            }

            convertView.setBackgroundColor(0);
            summaryView.setVisibility(summaryView.getText().toString().isEmpty() ? View.GONE : View.VISIBLE);

            return convertView;
        }
    }
}
