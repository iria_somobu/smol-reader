package com.somobu.smolreader.library;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.somobu.smolreader.R;

import org.geometerplus.android.fbreader.covers.CoverManager;
import org.geometerplus.android.fbreader.util.AndroidImageSynchronizer;
import org.geometerplus.android.util.ViewUtil;
import org.geometerplus.fbreader.book.Author;
import org.geometerplus.fbreader.book.Book;

import java.util.ArrayList;
import java.util.List;

public class BookAdapter extends ArrayAdapter<Book> {

    private final List<Book> books = new ArrayList<>();
    private final AndroidImageSynchronizer ImageSynchronizer;
    private CoverManager myCoverManager = null;

    public BookAdapter(@NonNull Context context, AndroidImageSynchronizer sync) {
        super(context, R.layout.library_tree_item);
        this.ImageSynchronizer = sync;
    }

    public void replace(List<Book> replacement) {
        this.books.clear();
        this.books.addAll(replacement);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return books.size();
    }

    @Override
    public Book getItem(int i) {
        return books.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public String getSummary(Book book) {
        StringBuilder builder = new StringBuilder();
        int count = 0;
        for (Author author : book.authors()) {
            if (count++ > 0) {
                builder.append(",  ");
            }
            builder.append(author.DisplayName);
            if (count == 5) {
                break;
            }
        }
        return builder.toString();
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.library_tree_item, parent, false);
        }

        Book book = getItem(position);

        final boolean unread = !book.hasLabel(Book.READ_LABEL);

        final TextView nameView = ViewUtil.findTextView(convertView, R.id.library_tree_item_name);
        nameView.setText(unread ? Html.fromHtml("<b>" + book.getTitle()) : book.getTitle());

        final TextView summaryView = ViewUtil.findTextView(convertView, R.id.library_tree_item_childrenlist);
        summaryView.setText(unread ? Html.fromHtml("<b>" + getSummary(book)) : getSummary(book));

        convertView.setBackgroundColor(0);

        if (myCoverManager == null) {
            convertView.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            myCoverManager = new CoverManager(getContext(), ImageSynchronizer,
                    convertView.getMeasuredWidth(),
                    convertView.getMeasuredHeight());
            myCoverManager.workaroundDisableParamsOverride = true;
            convertView.requestLayout();
        }

        final ImageView coverView = ViewUtil.findImageView(convertView, R.id.library_tree_item_icon);
        if (!myCoverManager.trySetCoverImage(coverView, book)) {
            coverView.setImageResource(R.drawable.ic_list_library_book);
        }

        summaryView.setVisibility(summaryView.getText().toString().isEmpty() ? View.GONE : View.VISIBLE);

        return convertView;
    }
}
