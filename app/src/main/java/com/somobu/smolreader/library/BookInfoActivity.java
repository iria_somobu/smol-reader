package com.somobu.smolreader.library;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.somobu.smolreader.ISynchronizerProvider;
import com.somobu.smolreader.R;
import com.somobu.smolreader.Theming;

import org.geometerplus.android.fbreader.util.AndroidImageSynchronizer;

public class BookInfoActivity extends Activity implements ISynchronizerProvider {

    private AndroidImageSynchronizer synchronizer = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        Theming.onCreate(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.v2_activity_empty);

        synchronizer = new AndroidImageSynchronizer(this);

        String path = getIntent().getStringExtra(BookInfoFragment.KEY_BOOK_FILE);
        BookInfoFragment fragment = BookInfoFragment.instance(path);
        getFragmentManager().beginTransaction().add(R.id.empty, fragment).commit();
    }

    @Override
    public AndroidImageSynchronizer getImageSynchronizer() {
        return synchronizer;
    }
}
