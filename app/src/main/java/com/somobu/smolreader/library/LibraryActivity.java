package com.somobu.smolreader.library;

import android.app.Activity;
import android.os.Bundle;

import com.somobu.smolreader.ISynchronizerProvider;
import com.somobu.smolreader.R;
import com.somobu.smolreader.Theming;

import org.geometerplus.android.fbreader.util.AndroidImageSynchronizer;

public class LibraryActivity extends Activity implements ISynchronizerProvider, ILibraryHost {

    private AndroidImageSynchronizer synchronizer = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Theming.onCreate(this);
        super.onCreate(savedInstanceState);

        synchronizer = new AndroidImageSynchronizer(this);
        setContentView(R.layout.v2_activity_library);
    }

    @Override
    public AndroidImageSynchronizer getImageSynchronizer() {
        return synchronizer;
    }

    @Override
    public void setSubtitle(String subtitle) {
        ((LibraryFragment) getFragmentManager().findFragmentById(R.id.lib_fragment)).subtitle(subtitle);
    }

    @Override
    public void notifyLibReload() {
        // TODO: me! me! me!
    }
}
