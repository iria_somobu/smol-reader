package com.somobu.smolreader.library;

import android.app.ListFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.somobu.smolreader.R;

public abstract class AbstractLibFragment extends ListFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.v2_listfragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        registerForContextMenu(getListView());
    }

    /**
     * Use {@link #onContextListItemSelected(MenuItem)} instead.
     */
    @Override
    public final boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        if (getListView().getPositionForView(info.targetView) == -1)
            return super.onContextItemSelected(item);

        onContextListItemSelected(item);

        return true;
    }

    public void onContextListItemSelected(MenuItem item) {

    }


    public abstract String getTitle();

    public abstract String getSubtitle();

    /**
     * @return null to disable
     */
    public abstract AbstractLibFragment.Action getActionIcon();


    public static class Action {

        public final int resource;
        public final String hint;
        public final Runnable onClick;

        public Action(int drawable, String hint, Runnable onClick) {
            this.resource = drawable;
            this.hint = hint;
            this.onClick = onClick;
        }

    }
}
