package com.somobu.smolreader.library;

import android.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.somobu.smolreader.R;
import com.somobu.smolreader.UglyStore;

public class LibraryFragment extends Fragment {

    private View root = null; // Am I doing it wrong?

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.v2_fragment_library, container, false);

        final AbstractLibFragment[] fragments = {
                new FavoritesFragment(),
                new RecentsFragment(),
                new CatalogsFragment(),
                new SearchFragment(),
                new NetworkFragment()
        };

        int[] icons = {
                R.drawable.ic_list_library_favorites,
                R.drawable.ic_list_library_recent,
                R.drawable.ic_list_library_folder,
                R.drawable.ic_list_library_search,
                R.drawable.ic_menu_networklibrary
        };

        ViewPager pager = root.findViewById(R.id.pager);
        pager.setAdapter(new PagerAdapter() {

            final View[] views = new View[fragments.length];

            @Override
            public int getCount() {
                return fragments.length;
            }

            @Override
            public Object instantiateItem(ViewGroup container, int position) {
                getFragmentManager().beginTransaction().add(container.getId(), fragments[position]).commit();
                getFragmentManager().executePendingTransactions();
                views[position] = container.getChildAt(container.getChildCount() - 1);
                return fragments[position];
            }

            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
                getFragmentManager().beginTransaction().remove(fragments[position]).commit();
                getFragmentManager().executePendingTransactions();
                views[position] = null;
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                int frIdx = -1;

                for (int i = 0; i < fragments.length; i++) {
                    if (fragments[i] == object) frIdx = i;
                }

                if (frIdx == -1) return false;
                else return view == views[frIdx];
            }
        });

        TabLayout tl = root.findViewById(R.id.tabs);
        tl.setupWithViewPager(pager);
        tl.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                AbstractLibFragment f = fragments[tab.getPosition()];
                title(f.getTitle(), f.getSubtitle());

                if (root != null) {
                    AbstractLibFragment.Action action = f.getActionIcon();
                    ImageButton btn = root.findViewById(R.id.action_button);
                    if (action == null) {
                        btn.setVisibility(View.GONE);
                    } else {
                        btn.setVisibility(View.VISIBLE);
                        btn.setImageResource(action.resource);
                        btn.setOnClickListener(view -> {
                            if (action.onClick != null) {
                                action.onClick.run();
                            }
                        });
                        btn.setOnLongClickListener(view -> {
                            Toast.makeText(getActivity(), action.hint, Toast.LENGTH_SHORT).show();
                            return true;
                        });
                    }

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        for (int i = 0; i < fragments.length; i++) {
            TabLayout.Tab t = tl.getTabAt(i);
            t.setIcon(icons[i]);
        }

        pager.setCurrentItem(2, false);

        return root;
    }

    private void title(String title, String subtitle) {
        if (root != null) {
            ((TextView) root.findViewById(R.id.title)).setText(title);

            TextView st = root.findViewById(R.id.subtitle);
            if (subtitle == null) {
                st.setVisibility(View.GONE);
            } else {
                st.setVisibility(View.VISIBLE);
                st.setText(subtitle);
            }
        }
    }

    public void subtitle(String subtitle) {
        System.out.println("Subtitle here was called");
        if (root != null) {
            TextView st = root.findViewById(R.id.subtitle);
            if (subtitle == null) {
                st.setVisibility(View.GONE);
            } else {
                st.setVisibility(View.VISIBLE);
                st.setText(subtitle);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        UglyStore.markReload();
    }
}
