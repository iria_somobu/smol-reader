package com.somobu.smolreader;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;

import org.geometerplus.android.fbreader.libraryService.BookCollectionShadow;
import org.geometerplus.android.fbreader.network.Util;
import org.geometerplus.android.fbreader.network.auth.ActivityNetworkContext;
import org.geometerplus.fbreader.network.NetworkLibrary;

public class UglyStore {

    private static final UglyStore singleton = new UglyStore();

    public static void local(Context context, LocalReady r) {
        Handler handler = new Handler(context.getMainLooper());

        new Thread() {

            @Override
            public void run() {
                if (!singleton.hasData) {
                    singleton.bind(context);

                    try {
                        synchronized (singleton.lock) {
                            singleton.lock.wait();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                handler.post(() -> r.then(singleton.collection));
            }
        }.start();
    }

    public static void network(Activity context, ActivityNetworkContext myNetworkContext, NetReady r) {
        Handler handler = new Handler(context.getMainLooper());
        UglyStore.local(context, store -> {
            NetworkLibrary library = Util.networkLibrary(context);

            Runnable action = () -> handler.post(() -> r.then(library));

            if (!library.isInitialized()) {
                Util.initLibrary(context, myNetworkContext, action);
            } else {
                action.run();
            }
        });
    }

    public static void markReload() {
        singleton.bindCalled = false;
        singleton.hasData = false;
    }

    private final Object lock = new Object();
    private boolean bindCalled = false;
    private boolean hasData = false;
    private final BookCollectionShadow collection = new BookCollectionShadow();

    private synchronized void bind(Context context) {
        if (bindCalled) return;
        bindCalled = true;

        collection.unbind();
        collection.bindToService(context, () -> {
            hasData = true;
            synchronized (lock) {
                lock.notifyAll();
            }
        });
    }

    public interface LocalReady {

        void then(BookCollectionShadow store);

    }

    public interface NetReady {

        void then(NetworkLibrary library);

    }
}
