package com.somobu.smolreader;

import org.geometerplus.android.fbreader.util.AndroidImageSynchronizer;

public interface ISynchronizerProvider {

    AndroidImageSynchronizer getImageSynchronizer();

}
