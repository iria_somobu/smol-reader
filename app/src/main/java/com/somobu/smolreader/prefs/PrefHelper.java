/*
 * Copyright (C) 2007-2015 FBReader.ORG Limited <contact@fbreader.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

package com.somobu.smolreader.prefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import org.geometerplus.zlibrary.core.util.ZLColor;

public class PrefHelper {

    private final SharedPreferences prefs;

    public PrefHelper(Context ctx) {
        this.prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    public String getTextSearchPattern(){
        return prefs.getString("TEXT_SEARCH_PATTERN", "");
    }

    public void setTextSearchPattern(String pattern){
        prefs.edit().putString("TEXT_SEARCH_PATTERN", pattern).apply();
    }

    public boolean isAdjustingBrightness() {
        return prefs.getBoolean("APPEAR_BRI_ADJUST", false);
    }

    public int getBrightnessLevelToScreenOff() {
        try {
            return Integer.parseInt(prefs.getString("BATTERY_SLEEP", "25"));
        } catch (Exception e) {
            return 25;
        }
    }

    public boolean showStatusBar() {
        return prefs.getBoolean("SHOW_SYSUI_STATUS_BAR", false);
    }

    public boolean showNavBar() {
        return prefs.getBoolean("SHOW_SYSUI_NAVBAR", false);
    }

    public boolean disableBacklights() {
        return prefs.getBoolean("APPEAR_BACKLIGHTS", true);
    }


    public int getScreenBrightness() {
        return prefs.getInt("SCREEN_BRIGHT", 0);
    }


    public int getToastFontSizePercent() {
        try {
            return Integer.parseInt(prefs.getString("POPUP_FONT_SIZE", "90"));
        } catch (Exception e) {
            return 90;
        }
    }

    public String getToastShowMode() {
        return prefs.getString("POPUP_LINKED", "footnotesAndSuperscripts");
    }

    public int getToastDuration() {
        try {
            return Integer.parseInt(prefs.getString("POPUP_DURATION", "5000"));
        } catch (Exception e) {
            return 5000;
        }
    }


    public String footerType() {
        return prefs.getString("PROGRESS_TYPE", "showAsFooterOldStyle");
    }

    public int footerHeight() {
        try {
            return Integer.parseInt(prefs.getString("PROGRESS_HEIGHT", "24"));
        } catch (Exception e) {
            return 24;
        }
    }

    public boolean footerShowTOCmarks() {
        return prefs.getBoolean("PROGRESS_TOC", true);
    }

    public boolean footerShowPage() {
        return prefs.getBoolean("PROGRESS_PAGE", true);
    }

    public boolean footerShowClock() {
        return prefs.getBoolean("PROGRESS_CLOCK", true);
    }

    public boolean footerShowBattery() {
        return prefs.getBoolean("PROGRESS_BATTERY", true);
    }

    public String footerFont() {
        return prefs.getString("PROGRESS_FONT", "inherit");
    }

    public ZLColor footerFill() {
        return new ZLColor(prefs.getInt("PROGRESS_OLD_COLOR", 0xff555555));
    }

    public ZLColor footerBackground() {
        return new ZLColor(prefs.getInt("PROGRESS_BACKGROUND_COLOR", 0xff444444));
    }

    public ZLColor footerForeground() {
        return new ZLColor(prefs.getInt("PROGRESS_FOREGROUND_COLOR", 0xffbbbbbb));
    }

    public ZLColor footerUnread() {
        return new ZLColor(prefs.getInt("PROGRESS_UNREAD_COLOR", 0xff777777));
    }


    public String getPageTurnMode() {
        return prefs.getString("PAGETURN_TOUCH_MODE", "both");
    }

    public boolean pageTurnDoubleTap() {
        return prefs.getBoolean("PAGETURN_DOUBLE_TAP", false);
    }

    public String getPageTurnAnimation() {
        return prefs.getString("PAGETURN_ANIMATION", "classic_slide");
    }

    public int getPageTurnSpeed() {
        try {
            return Integer.parseInt(prefs.getString("PAGETURN_SPEED", "8"));
        } catch (Exception e) {
            return 8;
        }
    }


    public boolean fontAntiAlias() {
        return prefs.getBoolean("FONT_ANTIALIAS", true);
    }

    public boolean fontKerning() {
        return prefs.getBoolean("FONT_KERNING", true);
    }

    public boolean fontDithering() {
        return prefs.getBoolean("FONT_DITHERING", true);
    }

    public boolean fontHinting() {
        return prefs.getBoolean("FONT_HINTING", true);
    }

    public boolean fontSubpixel() {
        return prefs.getBoolean("FONT_SUBPIXEL", true);
    }


    public int marinLeft() {
        try {
            return Integer.parseInt(prefs.getString("MARGIN_LEFT", "16"));
        } catch (Exception e) {
            return 16;
        }
    }

    public int marinRight() {
        try {
            return Integer.parseInt(prefs.getString("MARGIN_RIGHT", "16"));
        } catch (Exception e) {
            return 16;
        }
    }

    public int marinTop() {
        try {
            return Integer.parseInt(prefs.getString("MARGIN_TOP", "16"));
        } catch (Exception e) {
            return 16;
        }
    }

    public int marinBottom() {
        try {
            return Integer.parseInt(prefs.getString("MARGIN_BOTTOM", "16"));
        } catch (Exception e) {
            return 16;
        }
    }

    public int marinColumns() {
        try {
            return Integer.parseInt(prefs.getString("MARGIN_COLUMNS", "48"));
        } catch (Exception e) {
            return 48;
        }
    }


    public String imgFitMode() {
        return prefs.getString("IMG_FIT_MODE", "covers");
    }

    public ZLColor imgBgColor() {
        return new ZLColor(prefs.getInt("IMG_BG_COLOR", 0xffffffff));
    }

    public boolean imgMatchBg() {
        return prefs.getBoolean("IMG_MATCH_BG", false);
    }
}
