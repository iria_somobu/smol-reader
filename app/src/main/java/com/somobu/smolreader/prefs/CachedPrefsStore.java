package com.somobu.smolreader.prefs;

import android.content.SharedPreferences;

public class CachedPrefsStore {

    private static SharedPreferences preferences = null;

    public static CachedPrefsStore instance() {
        if (preferences == null) {
            throw new RuntimeException("U should init prefs before call instance()");
        }

        return new CachedPrefsStore(preferences);
    }

    public static void setPreferences(SharedPreferences prefs) {
        preferences = prefs;
    }

    private final SharedPreferences prefs;

    private CachedPrefsStore(SharedPreferences prefs) {
        this.prefs = prefs;
    }

    public SharedPreferences getPreferences(){
        return preferences;
    }

    public boolean cssFontFamily() {
        return prefs.getBoolean("CSS_FONT_FAMILY", false);
    }

    public boolean cssFontSize() {
        return prefs.getBoolean("CSS_FONT_SIZE", false);
    }

    public boolean cssTextAlignment() {
        return prefs.getBoolean("CSS_TEXT_ALIGNMENT", false);
    }

    public boolean cssMargins() {
        return prefs.getBoolean("CSS_MARGINS", false);
    }

}
