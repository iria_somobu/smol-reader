package com.somobu.smolreader.prefs;

public class FontsSettingsActivity extends AbsContextPrefActivity {

    @Override
    public Group[] listGroups() {
        return new Group[]{
                new Group("Regular paragraph", "p"),
                new Group("EPub paragraph", "p.xhtml"),
                new Group("Title", "title"),
                new Group("Section title", "section"),
                new Group("Subtitle", "subtitle"),
                new Group("Header 1", "h1"),
                new Group("Header 2", "h2"),
                new Group("Header 3", "h3"),
                new Group("Header 4", "h4"),
                new Group("Header 5", "h5"),
                new Group("Header 6", "h6"),
                new Group("Annotation", "annotation"),
                new Group("Epigraph", "epigraph"),
                new Group("Author", "author"),
                new Group("Poem title", "poem"),
                new Group("Stanza", "stanza"),
                new Group("Verse", "verse"),
                new Group("Cite", "cite"),
                new Group("Internal hyperlink", "a.internal"),
                new Group("External hyperlink", "a.external"),
                new Group("Footnote", "footnote"),
                new Group("Italic", "i"),
                new Group("Emphasis", "em"),
                new Group("Bold", "b"),
                new Group("Strong", "strong"),
                new Group("Definition", "dfn"),
                new Group("Definition description", "dd"),
                new Group("Preformatted text", "pre"),
                new Group("Code", "code"),
                new Group("Strikethrough", "strike"),
                new Group("Superscript", "sup"),
                new Group("Subscript", "sub"),
                new Group("Image", "image"),
                new Group("Date", "date")
        };
    }

    @Override
    public Pref[] listPrefs(String id) {
        return new Pref[]{
                new StringArrayPref("Font family", id + "::" + StylesHelper.font_family,
                        new String[]{"<unchanged>"},
                        new String[]{"default"}),
                new StringPref("Font size", id + "::" + StylesHelper.font_size, "E.g.: 20px, 10pt, 0.8em, 1ex, 120%%"),
                new StringArrayPref("Bold", id + "::" + StylesHelper.font_weight,
                        new String[]{"Normal", "Bold", "<unchanged>"},
                        new String[]{"normal", "bold", "default"}),
                new StringArrayPref("Italic", id + "::" + StylesHelper.font_style,
                        new String[]{"Normal", "Italic", "<unchanged>"},
                        new String[]{"normal", "italic", "default"}),
                new StringArrayPref("Text decoration", id + "::" + StylesHelper.text_decoration,
                        new String[]{"No decorations", "Underlined", "Strikethrough", "<unchanged>"},
                        new String[]{"undecorated", "underline", "line-through", "inherit"}),
                new StringArrayPref("Allow hyphenation", id + "::" + StylesHelper.hyphens,
                        new String[]{"Yes", "No", "<unchanged>"},
                        new String[]{"auto", "none", "default"}),
                new StringArrayPref("Alignment", id + "::" + StylesHelper.text_align,
                        new String[]{"Left", "Right", "Centered", "Justified", "<unchanged>"},
                        new String[]{"left", "right", "center", "justify", "default"}),
                new StringPref("Line spacing", id + "::" + StylesHelper.line_height, "E.g.: 120%"),
                new StringPref("Space before paragraph", id + "::" + StylesHelper.margin_top, "E.g.: 20px, -10pt, 0.8em, 1ex, 120%%"),
                new StringPref("Space after paragraph", id + "::" + StylesHelper.margin_bottom, "E.g.: 20px, -10pt, 0.8em, 1ex, 120%%"),
                new StringPref("Left margin", id + "::" + StylesHelper.margin_left, "E.g.: 20px, -10pt, 0.8em, 1ex, 120%%"),
                new StringPref("Right margin", id + "::" + StylesHelper.margin_right, "E.g.: 20px, -10pt, 0.8em, 1ex, 120%%"),
//                new StringPref("First line", "", "E.g.: 20px, -10pt, 0.8em, 1ex, 120%%"),
                new StringPref("Vertical alignment", id + "::" + StylesHelper.vertical_align, "E.g.: 20px, -10pt, 0.8em, 1ex, 120%%")
        };
    }
}
