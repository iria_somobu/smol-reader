package com.somobu.smolreader.prefs;

import org.geometerplus.fbreader.fbreader.options.ColorProfile;

public class ProfilesSettingsActivity extends AbsContextPrefActivity {

    @Override
    public Group[] listGroups() {
        return new Group[]{
                new Group("Day profile", ColorProfile.DAY),
                new Group("Night profile", ColorProfile.NIGHT)
        };
    }

    @Override
    public Pref[] listPrefs(String id) {
        return new Pref[]{
                new StringArrayPref("Background", id + ":BG",
                        new String[]{"Hard paper", "Leather", "Sand", "Sepia", "Wood", "Solid color"},
                        new String[]{"paper", "leather", "sand", "sepia", "wood", ""}),
                new ColorPref("Background solid color", id + ":BG_COLOR"),
                new ColorPref("Regular text", id + ":REGULAR"),
                new ColorPref("Hyperlink text", id + ":HYPERLINK"),
                new ColorPref("Visited link text", id + ":VISITED"),
                new ColorPref("Selection background", id + ":SELECTION_BG"),
                new ColorPref("Selection text", id + ":SELECTION_FG"),
                new ColorPref("Search result background", id + ":SEARCH_BG"),
                new ColorPref("Search result text", id + ":SEARCH_FG")
        };
    }
}
