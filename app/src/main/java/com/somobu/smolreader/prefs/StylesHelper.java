package com.somobu.smolreader.prefs;

import java.util.HashMap;
import java.util.Map;

public class StylesHelper {

    public static final String id = "fbreader-id";
    public static final String name = "fbreader-name";
    public static final String font_family = "font-family";
    public static final String font_size = "font-size";
    public static final String font_weight = "font-weight";
    public static final String font_style = "font-style";
    public static final String text_decoration = "text-decoration";
    public static final String hyphens = "hyphens";
    public static final String margin_top = "margin-top";
    public static final String margin_bottom = "margin-bottom";
    public static final String margin_left = "margin-left";
    public static final String margin_right = "margin-right";
    public static final String text_ident = "text-indent";
    public static final String text_align = "text-align";
    public static final String vertical_align = "vertical-align";
    public static final String line_height = "line-height";

    private HashMap<String, Map<String, String>> defaultStyles = new HashMap<>();

    private void appendBlock(String name, String[] data) {

        HashMap<String, String> dataMap = new HashMap<>();
        for (int i = 0; i < data.length - 1; i += 2) {
            dataMap.put(data[i], data[i + 1]);
        }

        defaultStyles.put(name, dataMap);
    }

    /**
     * This is an ugly hardcode replacement for a former assets/styles.css and assets/styles.xml
     * files.
     */
    public Map<String, Map<String, String>> getDefault() {
        defaultStyles = new HashMap<>();

        appendBlock("p", new String[]{
                id, "0",
                name, "Regular Paragraph",
                text_ident, "10pt",
                hyphens, "auto"
        });

        appendBlock("p.xhtml", new String[]{
                id, "51",
                name, "xhtml-tag-p",
                text_ident, "10pt",
                margin_top, "0.2em",
                margin_bottom, "0.2em",
                hyphens, "auto"
        });

        appendBlock("title", new String[]{
                id, "1",
                name, "Title",
                font_size, "1.5em",
                font_weight, "bold",
                margin_top, "0.2em",
                margin_bottom, "0.5em",
                text_align, "center",
                hyphens, "none"
        });

        appendBlock("section", new String[]{
                id, "2",
                name, "Section Title",
                font_size, "1.2em",
                font_weight, "bold",
                margin_bottom, "0.4em",
                text_align, "center",
                hyphens, "none"
        });

        appendBlock("subtitle", new String[]{
                id, "4",
                name, "Subtitle",
                font_weight, "bold"
        });

        appendBlock("h1", new String[]{
                id, "31",
                name, "Header 1",
                font_size, "2em",
                font_weight, "bold",
                margin_top, "0.2em",
                margin_bottom, "0.2em",
                hyphens, "none"
        });

        appendBlock("h2", new String[]{
                id, "32",
                name, "Header 2",
                font_size, "1.5em",
                font_weight, "bold",
                margin_top, "0.2em",
                margin_bottom, "0.2em",
                hyphens, "none"
        });

        appendBlock("h3", new String[]{
                id, "33",
                name, "Header 3",
                font_size, "1.17em",
                font_weight, "bold",
                margin_top, "0.2em",
                margin_bottom, "0.2em"
        });

        appendBlock("h4", new String[]{
                id, "34",
                name, "Header 4",
                font_weight, "bold",
                margin_top, "0.2em",
                margin_bottom, "0.2em"
        });

        appendBlock("h5", new String[]{
                id, "35",
                name, "Header 5",
                font_size, "0.83em",
                font_weight, "bold",
                margin_top, "0.22em",
                margin_bottom, "0.22em"
        });

        appendBlock("h6", new String[]{
                id, "36",
                name, "Header 6",
                font_size, "0.67em",
                font_weight, "bold",
                margin_top, "0.3em",
                margin_bottom, "0.3em"
        });

        appendBlock("annotation", new String[]{
                id, "5",
                name, "Annotation",
                font_size, "0.91em",
                text_ident, "10pt",
                hyphens, "auto"
        });

        appendBlock("epigraph", new String[]{
                id, "6",
                name, "Epigraph",
                font_size, "0.91em",
                font_style, "italic",
                margin_right, "40pt",
                hyphens, "auto"
        });

        appendBlock("author", new String[]{
                id, "13",
                name, "Author",
                margin_right, "20pt",
                hyphens, "none"
        });

        appendBlock("poem", new String[]{
                id, "3",
                name, "Poem Title",
                font_size, "1.09em",
                font_weight, "bold",
                margin_top, "0.25em",
                margin_bottom, "0.25em",
                margin_right, "30pt",
                hyphens, "none"
        });

        appendBlock("stanza", new String[]{
                id, "7",
                name, "Stanza",
                margin_top, "0.3em",
                margin_bottom, "0.3em",
                text_align, "left",
                hyphens, "none"
        });

        appendBlock("verse", new String[]{
                id, "8",
                name, "Verse",
                margin_right, "20pt",
                text_align, "left",
                hyphens, "none"
        });

        appendBlock("cite", new String[]{
                id, "12",
                name, "Cite",
                font_style, "italic"
        });

        appendBlock("a.internal", new String[]{
                id, "15",
                name, "Internal Hyperlink",
                hyphens, "none",
                text_decoration, "underline"
        });

        appendBlock("a.external", new String[]{
                id, "37",
                name, "External Hyperlink",
                hyphens, "none",
                text_decoration, "underline"
        });

        appendBlock("footnote", new String[]{
                id, "16",
                name, "Footnote",
                font_size, "0.7em",
                vertical_align, "0.5em",
                hyphens, "none"
        });

        appendBlock("i", new String[]{
                id, "27",
                name, "Italic",
                font_style, "italic"
        });

        appendBlock("em", new String[]{
                id, "17",
                name, "Emphasis",
                font_style, "italic"
        });

        appendBlock("b", new String[]{
                id, "28",
                name, "Bold",
                font_weight, "bold"
        });

        appendBlock("strong", new String[]{
                id, "18",
                name, "Strong",
                font_weight, "bold"
        });

        appendBlock("dfn", new String[]{
                id, "29",
                name, "Definition",
                font_style, "italic"
        });

        appendBlock("dd", new String[]{
                id, "30",
                name, "Definition Description",
                font_style, "italic"
        });

        appendBlock("pre", new String[]{
                id, "9",
                name, "Preformatted text",
                font_family, "Monospace",
                text_align, "left",
                hyphens, "none",
                margin_top, "1em",
                margin_bottom, "1em"
        });

        appendBlock("code", new String[]{
                id, "21",
                name, "Code",
                font_family, "Monospace",
                hyphens, "none"
        });

        appendBlock("strike", new String[]{
                id, "22",
                name, "StrikeThrough",
                text_decoration, "line-through"
        });

        appendBlock("sup", new String[]{
                id, "20",
                name, "Superscript",
                font_size, "0.7em",
                vertical_align, "0.5em",
                hyphens, "none"
        });

        appendBlock("sub", new String[]{
                id, "19",
                name, "Subscript",
                font_size, "0.7em",
                vertical_align, "-0.5em",
                hyphens, "none"
        });

        appendBlock("image", new String[]{
                id, "10",
                name, "Image",
                margin_top, "0.4em",
                text_align, "center"
        });

        appendBlock("date", new String[]{
                id, "14",
                name, "Date",
                margin_right, "30pt",
                hyphens, "none"
        });

        return defaultStyles;
    }


}
