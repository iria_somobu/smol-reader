package com.somobu.smolreader.prefs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.somobu.smolreader.R;
import com.somobu.smolreader.Theming;

import yuku.ambilwarna.AmbilWarnaDialog;
import yuku.ambilwarna.widget.AmbilWarnaPrefWidgetView;

public abstract class AbsContextPrefActivity extends Activity {

    public static class Group {

        public String title;
        public String id;

        public Group(String title, String id) {
            this.title = title;
            this.id = id;
        }

        @Override
        public String toString() {
            return title;
        }
    }

    public abstract static class Pref {

        public String title;
        public String key;

        public Pref(String title, String key) {
            this.title = title;
            this.key = key;
        }

        public String fancyValue(SharedPreferences prefs) {
            return prefs.getString(key, getDefaultValue());
        }

        public String getDefaultValue() {
            return "";
        }

        public abstract void onClick(AbsContextPrefActivity ctx, SharedPreferences prefs);
    }

    public static class ColorPref extends Pref {

        public ColorPref(String title, String key) {
            super(title, key);
        }

        @Override
        public void onClick(AbsContextPrefActivity ctx, SharedPreferences prefs) {
            new AmbilWarnaDialog(ctx, prefs.getInt(key, 0),
                    new AmbilWarnaDialog.OnAmbilWarnaListener() {
                        @Override
                        public void onOk(AmbilWarnaDialog dialog, int color) {
                            prefs.edit().putInt(key, color).apply();
                            ctx.notifyPrefChange();
                        }

                        @Override
                        public void onCancel(AmbilWarnaDialog dialog) {
                        }
                    }
            ).show();
        }
    }

    public static class StringPref extends Pref {

        public String hint;

        public StringPref(String title, String key, String hint) {
            super(title, key);
            this.hint = hint;
        }

        @Override
        public void onClick(AbsContextPrefActivity ctx, SharedPreferences prefs) {
            View root = LayoutInflater.from(ctx).inflate(R.layout.v2_dialog_pref_string, null);
            String text = prefs.getString(key, getDefaultValue());
            ((EditText) root.findViewById(R.id.field)).setText(text);
            ((TextView) root.findViewById(R.id.hint)).setText(hint);
            new AlertDialog.Builder(ctx)
                    .setTitle(title)
                    .setView(root)
                    .setNegativeButton("Cancel", null)
                    .setPositiveButton("Ok", (dialogInterface, i) -> {
                        String t = ((EditText) root.findViewById(R.id.field)).getText().toString();
                        prefs.edit().putString(key, t).apply();
                        ctx.notifyPrefChange();
                    })
                    .show();
        }
    }

    public static class StringArrayPref extends Pref {

        public String[] titles;
        public String[] values;

        public StringArrayPref(String title, String key, String[] titles, String[] values) {
            super(title, key);
            this.titles = titles;
            this.values = values;
        }

        @Override
        public String fancyValue(SharedPreferences prefs) {
            String value = prefs.getString(key, getDefaultValue());
            for (int i = 0; i < values.length; i++) {
                if (values[i].equals(value)) {
                    return titles[i];
                }
            }
            return value;
        }

        @Override
        public void onClick(AbsContextPrefActivity ctx, SharedPreferences prefs) {
            new AlertDialog.Builder(ctx)
                    .setTitle(title)
                    .setItems(titles, (dialogInterface, i) -> {
                        prefs.edit().putString(key, values[i]).apply();
                        ctx.notifyPrefChange();
                    }).show();
        }
    }

    private SharedPreferences prefs;
    private PrefAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        Theming.onCreate(this);
        super.onCreate(savedInstanceState);
        prefs = PreferenceManager.getDefaultSharedPreferences(AbsContextPrefActivity.this);

        setContentView(R.layout.v2_activity_context_pref);

        Spinner sp = findViewById(R.id.spinner);
        Group[] groupList = listGroups();
        sp.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, groupList));

        ListView list = findViewById(R.id.list);
        adapter = new PrefAdapter();
        list.setAdapter(adapter);

        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                adapter.setData(listPrefs(groupList[i].id));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        list.setOnItemClickListener((adapterView, view, i, l) -> {
            adapter.getItem(i).onClick(AbsContextPrefActivity.this, prefs);
        });

        if (groupList.length > 0) {
            adapter.setData(listPrefs(groupList[0].id));
            sp.setSelection(0, false);
        }
    }

    void notifyPrefChange() {
        adapter.notifyDataSetChanged();
    }

    private class PrefAdapter extends android.widget.BaseAdapter {

        private Pref[] data = {};

        public void setData(Pref[] data) {
            this.data = data;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return data.length;
        }

        @Override
        public Pref getItem(int i) {
            return data[i];
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = LayoutInflater.from(AbsContextPrefActivity.this).inflate(R.layout.v2_item_pref, viewGroup, false);
            }

            Pref item = getItem(i);
            ((TextView) view.findViewById(R.id.title)).setText(item.title);

            TextView t = view.findViewById(R.id.value);
            AmbilWarnaPrefWidgetView p = view.findViewById(R.id.color_preview);
            if (item instanceof ColorPref) {
                t.setVisibility(View.GONE);
                p.setVisibility(View.VISIBLE);
                p.setBackgroundColor(prefs.getInt(item.key, 0));
            } else {
                t.setVisibility(View.VISIBLE);
                p.setVisibility(View.GONE);
                t.setText(item.fancyValue(prefs));
            }

            return view;
        }
    }

    public abstract Group[] listGroups();

    public abstract Pref[] listPrefs(String id);

}
