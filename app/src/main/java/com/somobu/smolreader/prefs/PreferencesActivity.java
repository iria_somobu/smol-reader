package com.somobu.smolreader.prefs;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.KeyEvent;

import com.somobu.smolreader.R;
import com.somobu.smolreader.Theming;
import com.somobu.smolreader.UglyStore;

import org.geometerplus.fbreader.Paths;
import org.geometerplus.fbreader.fbreader.ActionCode;
import org.geometerplus.zlibrary.core.application.ZLKeyBindings;

public class PreferencesActivity extends PreferenceActivity {

    interface OneArgPrefL {

        void onPrefChange(Object o);

    }

    private final ZLKeyBindings keyBindings = new ZLKeyBindings();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        Theming.onCreate(this);
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preferences);

        findPreference("ABOUT_VER").setSummary(getVerInfo());

        subChange("DIR_BOOKS", () -> UglyStore.local(PreferencesActivity.this, store -> {
            store.reset(true);
            store.unbind();
            Paths.invalidatePathsCache(this);
            UglyStore.markReload();
        }));

        subChangeImm("PROGRESS_TYPE", (o) -> {
            String val = (String) o;

            boolean isOldFooter = "showAsFooterOldStyle".equals(val);
            boolean isNewFooter = "showAsFooter".equals(val);
            boolean isFooter = isOldFooter || isNewFooter;

            findPreference("PROGRESS_HEIGHT").setEnabled(isFooter);
            findPreference("PROGRESS_TOC").setEnabled(isFooter);
            findPreference("PROGRESS_PAGE").setEnabled(isFooter);
            findPreference("PROGRESS_CLOCK").setEnabled(isFooter);
            findPreference("PROGRESS_BATTERY").setEnabled(isFooter);
            findPreference("PROGRESS_FONT").setEnabled(isFooter);

            findPreference("PROGRESS_OLD_COLOR").setEnabled(isOldFooter);
            findPreference("PROGRESS_BACKGROUND_COLOR").setEnabled(isNewFooter);
            findPreference("PROGRESS_FOREGROUND_COLOR").setEnabled(isNewFooter);
            findPreference("PROGRESS_UNREAD_COLOR").setEnabled(isNewFooter);

        });


        subChange("PAGETURN_USE_VOLUME", (o) -> chVolume((boolean) o, false));
        subChange("PAGETURN_INVERT_VOLUME", (o) -> {
            boolean enabled = ((CheckBoxPreference) findPreference("PAGETURN_USE_VOLUME")).isChecked();
            chVolume(enabled, (boolean) o);
        });
        boolean pageturnEnabled = ((CheckBoxPreference) findPreference("PAGETURN_USE_VOLUME")).isChecked();
        findPreference("PAGETURN_INVERT_VOLUME").setEnabled(pageturnEnabled);


        subChange("BACK_KEY_ACTION", (o -> keyBindings.getOption(KeyEvent.KEYCODE_BACK, false).setValue((String) o)));
        subChange("BACK_KEY_LONG_ACTION", (o -> keyBindings.getOption(KeyEvent.KEYCODE_BACK, true).setValue((String) o)));
    }

    private String getVerInfo() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), 0);
            return info.versionName + " (code " + info.versionCode + ")";
        } catch (PackageManager.NameNotFoundException e) {
            return "N/A";
        }
    }

    private void chVolume(boolean enabled, boolean inverted) {
        CheckBoxPreference prefInvert = (CheckBoxPreference) findPreference("PAGETURN_INVERT_VOLUME");

        prefInvert.setEnabled(enabled);

        if (enabled) {
            if (inverted) {
                keyBindings.bindKey(KeyEvent.KEYCODE_VOLUME_DOWN, false, ActionCode.VOLUME_KEY_SCROLL_BACK);
                keyBindings.bindKey(KeyEvent.KEYCODE_VOLUME_UP, false, ActionCode.VOLUME_KEY_SCROLL_FORWARD);
            } else {
                keyBindings.bindKey(KeyEvent.KEYCODE_VOLUME_DOWN, false, ActionCode.VOLUME_KEY_SCROLL_FORWARD);
                keyBindings.bindKey(KeyEvent.KEYCODE_VOLUME_UP, false, ActionCode.VOLUME_KEY_SCROLL_BACK);
            }
        } else {
            keyBindings.bindKey(KeyEvent.KEYCODE_VOLUME_DOWN, false, "");
            keyBindings.bindKey(KeyEvent.KEYCODE_VOLUME_UP, false, "");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Paths.invalidatePathsCache(this);
    }

    private void subChangeImm(String key, OneArgPrefL r) {
        subChange(key, r);
        if (r != null) {
            Object o = PreferenceManager.getDefaultSharedPreferences(this)
                    .getAll().get(key);
            r.onPrefChange(o);
        }
    }

    private void subChange(String key, Runnable r) {
        if (r == null) return;
        findPreference(key).setOnPreferenceChangeListener((preference, o) -> {
            r.run();
            return true;
        });
    }

    private void subChange(String key, OneArgPrefL r) {
        if (r == null) return;
        findPreference(key).setOnPreferenceChangeListener((preference, o) -> {
            r.onPrefChange(o);
            return true;
        });
    }
}
