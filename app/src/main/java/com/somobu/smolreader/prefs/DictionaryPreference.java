package com.somobu.smolreader.prefs;

import android.content.Context;
import android.preference.ListPreference;
import android.preference.PreferenceManager;
import android.util.AttributeSet;

import org.geometerplus.android.fbreader.dict.DictionaryUtil;

import java.util.List;

public class DictionaryPreference extends ListPreference {

    private final String[] texts;
    private final String[] values;

    public DictionaryPreference(Context context, AttributeSet attrs) {
        super(context, attrs);

        List<DictionaryUtil.PackageInfo> infos = DictionaryUtil.dictionaryInfos(context,
                "DICT_DICTIONARY".equals(getKey()) // Small shortcut -- I dont want to pass custom value
        );
        texts = new String[infos.size()];
        values = new String[infos.size()];

        int index = 0;
        for (DictionaryUtil.PackageInfo i : infos) {
            texts[index] = i.getTitle();
            values[index] = i.getId();
            ++index;
        }

        setEntries(texts);
        setEntryValues(values);
        setDefaultValue(values[0]);
        updateSummary();
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);
        updateSummary();
    }

    private void updateSummary() {
        String value = PreferenceManager.getDefaultSharedPreferences(getContext()).getString(getKey(), values[0]);
        setSummary(texts[findIndexOfValue(value)]);
    }
}
