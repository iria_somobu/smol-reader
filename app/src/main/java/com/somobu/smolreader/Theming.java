package com.somobu.smolreader;

import android.app.Activity;

public class Theming {

    private static boolean isEbook() {
        String deviceName = android.os.Build.MODEL;
        String deviceMan = android.os.Build.MANUFACTURER;
        return "Onyx".equals(deviceMan);
    }

    public static void onCreate(Activity activity) {
        activity.setTheme(isEbook()
                ? R.style.Smolreader_Activity_EReader
                : R.style.Smolreader_Activity_Dark);
    }

    public static void onCreateDialogActivity(Activity activity) {
        activity.setTheme(isEbook()
                ? R.style.Smolreader_Dialog_EReader
                : R.style.Smolreader_Dialog_Dark);
    }

}
