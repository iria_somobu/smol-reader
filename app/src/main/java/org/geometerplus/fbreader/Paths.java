/*
 * Copyright (C) 2007-2015 FBReader.ORG Limited <contact@fbreader.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

package org.geometerplus.fbreader;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;

import org.geometerplus.zlibrary.core.util.SystemInfo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public abstract class Paths {

    public static List<String> getBooksPathList(Context ctx) {
        String s = PreferenceManager.getDefaultSharedPreferences(ctx)
                .getString("DIR_BOOKS", "/storage/emulated/0/Books");

        String[] list = s.split("\n");
        List<String> l = new ArrayList<>();
        for (String a : list) {
            if (!a.isEmpty()) l.add(a);
        }
        return l;
    }

    private static String cachedFontsPath = null;
    private static String cachedDownloadsPath = null;

    /**
     * TODO: this is an ugly hack. Remove me!
     */
    public static String getCachedDownloadsDir() {
        if (cachedFontsPath == null)
            throw new RuntimeException("Please invalidate cached paths");
        return cachedDownloadsPath;
    }

    /**
     * TODO: this is an ugly hack. Remove me!
     */
    public static String getFontsPathCached() {
        if (cachedFontsPath == null)
            throw new RuntimeException("Please invalidate cached paths");
        return cachedFontsPath;
    }

    /**
     * TODO: this is an ugly hack. Remove me!
     */
    public static void invalidatePathsCache(Context ctx) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(ctx);
        cachedFontsPath = sp.getString("DIR_FONTS", "/storage/emulated/0/Fonts");
        cachedDownloadsPath = sp.getString("DIR_DOWNLOADS", "/storage/emulated/0/Books");
    }

    public static String getDownloadsDir(Context ctx) {
        invalidatePathsCache(ctx);
        return cachedDownloadsPath;
    }

    public static String getFontsPath(Context ctx) {
        invalidatePathsCache(ctx);
        return getFontsPathCached();
    }

    private static String getExternalCacheDirPath(Context context) {
        final File d = context != null ? context.getExternalFilesDir(null) : null;
        if (d != null) {
            d.mkdirs();
            if (d.exists() && d.isDirectory()) {
                return d.getPath();
            }
        }
        return null;
    }

    private static String internalTempDirectoryValue(Context context) {
        final String dir = getExternalCacheDirPath(context);
        return dir != null ? dir : mainBookDirectory(context) + "/.FBReader";
    }


    public static String cardDirectory() {
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            return Environment.getExternalStorageDirectory().getPath();
        }

        final List<String> dirNames = new LinkedList<String>();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("/proc/self/mounts"));
            String line;
            while ((line = reader.readLine()) != null) {
                final String[] parts = line.split("\\s+");
                if (parts.length >= 4 &&
                        parts[2].toLowerCase().indexOf("fat") >= 0 &&
                        parts[3].indexOf("rw") >= 0) {
                    final File fsDir = new File(parts[1]);
                    if (fsDir.isDirectory() && fsDir.canWrite()) {
                        dirNames.add(fsDir.getPath());
                    }
                }
            }
        } catch (Throwable e) {
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                }
            }
        }

        for (String dir : dirNames) {
            if (dir.toLowerCase().indexOf("media") > 0) {
                return dir;
            }
        }
        if (dirNames.size() > 0) {
            return dirNames.get(0);
        }

        return Environment.getExternalStorageDirectory().getPath();
    }

    private static String defaultBookDirectory() {
        return cardDirectory() + "/Books";
    }

    public static List<String> bookPath(Context ctx) {
        final List<String> path = getBooksPathList(ctx);
        final String downloadsDirectory = getDownloadsDir(ctx);
        if (!"".equals(downloadsDirectory) && !path.contains(downloadsDirectory)) {
            path.add(downloadsDirectory);
        }
        return path;
    }

    public static String mainBookDirectory(Context ctx) {
        final List<String> bookPath = getBooksPathList(ctx);
        return bookPath.isEmpty() ? defaultBookDirectory() : bookPath.get(0);
    }

    public static SystemInfo systemInfo(Context context) {
        final Context appContext = context.getApplicationContext();
        return new SystemInfo() {
            public String tempDirectory() {
                return internalTempDirectoryValue(appContext);
            }

            public String networkCacheDirectory() {
                return tempDirectory() + "/cache";
            }
        };
    }

    public static String systemShareDirectory() {
        return "/system/usr/share/FBReader";
    }
}
