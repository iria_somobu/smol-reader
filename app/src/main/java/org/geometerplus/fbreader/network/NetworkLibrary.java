/*
 * Copyright (C) 2010-2015 FBReader.ORG Limited <contact@fbreader.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

package org.geometerplus.fbreader.network;

import org.geometerplus.fbreader.network.tree.AddCustomCatalogItemTree;
import org.geometerplus.fbreader.network.tree.ManageCatalogsItemTree;
import org.geometerplus.fbreader.network.tree.NetworkCatalogRootTree;
import org.geometerplus.fbreader.network.tree.NetworkCatalogTree;
import org.geometerplus.fbreader.network.tree.NetworkItemsLoader;
import org.geometerplus.fbreader.network.tree.RootTree;
import org.geometerplus.fbreader.network.tree.SearchCatalogTree;
import org.geometerplus.fbreader.network.urlInfo.UrlInfo;
import org.geometerplus.fbreader.tree.FBTree;
import org.geometerplus.zlibrary.core.image.ZLImage;
import org.geometerplus.zlibrary.core.network.ZLNetworkContext;
import org.geometerplus.zlibrary.core.network.ZLNetworkException;
import org.geometerplus.zlibrary.core.resources.ZLResource;
import org.geometerplus.zlibrary.core.util.MimeType;
import org.geometerplus.zlibrary.core.util.SystemInfo;
import org.geometerplus.zlibrary.core.util.ZLNetworkUtil;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class NetworkLibrary {
	public interface ChangeListener {
		public enum Code {
			InitializationFinished,
			InitializationFailed,
			SomeCode,
			/*
			ItemAdded,
			ItemRemoved,
			StatusChanged,
			*/
			SignedIn,
			Found,
			NotFound,
			EmptyCatalog,
			NetworkError
		}

		void onLibraryChanged(Code code, Object[] params);
	}

	private static NetworkLibrary ourInstance;

	public static NetworkLibrary Instance(SystemInfo systemInfo) {
		if (ourInstance == null) {
			ourInstance = new NetworkLibrary(systemInfo);
		}
		return ourInstance;
	}

	public static ZLResource resource() {
		return ZLResource.resource("networkLibrary");
	}


	public final SystemInfo SystemInfo;

	// that's important to keep this list synchronized
	// it can be used from background thread
	private final List<INetworkLink> myLinks = Collections.synchronizedList(new ArrayList<>());
	private final Set<ChangeListener> myListeners = Collections.synchronizedSet(new HashSet<>());
	private final Map<NetworkTree,NetworkItemsLoader> myLoaders = Collections.synchronizedMap(new HashMap<>());

	private final Map<String,WeakReference<ZLImage>> myImageMap = Collections.synchronizedMap(new HashMap<>());

	public List<String> allIds() {
		final ArrayList<String> ids = new ArrayList<String>();
		synchronized (myLinks) {
			for (INetworkLink link : myLinks) {
				ids.add(link.getUrl(UrlInfo.Type.Catalog));
			}
		}
		return ids;
	}

	List<INetworkLink> activeLinks() {
		final Map<String,INetworkLink> linksById = new TreeMap<String,INetworkLink>();
		synchronized (myLinks) {
			for (INetworkLink link : myLinks) {
				final String id = link.getUrl(UrlInfo.Type.Catalog);
				if (id != null) {
					linksById.put(id, link);
				}
			}
		}

		final List<INetworkLink> result = new LinkedList<INetworkLink>();
		for (String id : allIds()) {
			final INetworkLink link = linksById.get(id);
			if (link != null) {
				result.add(link);
			}
		}
		return result;
	}

	public INetworkLink getLinkByUrl(String url) {
		if (url == null) {
			return null;
		}
		synchronized (myLinks) {
			for (INetworkLink link : myLinks) {
				if (url.equals(link.getUrlInfo(UrlInfo.Type.Catalog).Url)) {
					return link;
				}
			}
		}
		return null;
	}

	private final RootTree myRootAllTree = new RootTree(this, "@AllRoot", false);
	private final RootTree myRootTree = new RootTree(this, "@Root", false);
	private final RootTree myFakeRootTree = new RootTree(this, "@FakeRoot", true);

	private boolean myChildrenAreInvalid = true;
	private boolean myUpdateVisibility;

	private volatile boolean myIsInitialized;

	private final SearchItem mySearchItem = new AllCatalogsSearchItem(this);

	private NetworkLibrary(SystemInfo systemInfo) {
		SystemInfo = systemInfo;
	}

	public boolean isInitialized() {
		return myIsInitialized;
	}

	public synchronized void initialize(ZLNetworkContext nc) throws ZLNetworkException {
		if (myIsInitialized) {
			return;
		}

		final NetworkDatabase db = NetworkDatabase.Instance();
		if (db != null) {
			myLinks.addAll(db.listLinks());
		}

		synchronize();

		myIsInitialized = true;
		fireModelChangedEvent(ChangeListener.Code.InitializationFinished);
	}


	public String rewriteUrl(String url, boolean externalUrl) {
		final String host = ZLNetworkUtil.hostFromUrl(url).toLowerCase();
		synchronized (myLinks) {
			for (INetworkLink link : myLinks) {
				if (link instanceof IPredefinedNetworkLink &&
					((IPredefinedNetworkLink)link).servesHost(host)) {
					url = link.rewriteUrl(url, externalUrl);
				}
			}
		}
		return url;
	}

	private void invalidateChildren() {
		myChildrenAreInvalid = true;
	}

	public void invalidateVisibility() {
		myUpdateVisibility = true;
	}

	private void makeUpToDateRootAll() {
		myRootAllTree.clear();
		synchronized (myLinks) {
			for (INetworkLink link : myLinks) {
				for (FBTree t : myRootAllTree.subtrees()) {
					final INetworkLink l = ((NetworkTree)t).getLink();
					if (l != null && link.compareTo(l) <= 0) {
						break;
					}
				}
				new NetworkCatalogRootTree(myRootAllTree, link);
			}
		}
	}

	private void makeUpToDate() {
		final Map<INetworkLink,List<NetworkCatalogTree>> linkToTreeMap = new HashMap<>();
		for (FBTree tree : myRootTree.subtrees()) {
			if (tree instanceof NetworkCatalogTree) {
				final NetworkCatalogTree nTree = (NetworkCatalogTree)tree;
				final INetworkLink link = nTree.getLink();
				if (link != null) {
					List<NetworkCatalogTree> list = linkToTreeMap.get(link);
					if (list == null) {
						list = new LinkedList<NetworkCatalogTree>();
						linkToTreeMap.put(link, list);
					}
					list.add(nTree);
				}
			}
		}

		if (!myRootTree.hasChildren()) {
			//new RecentCatalogListTree(
			//	myRootTree, new RecentCatalogListItem(resource().getResource("recent"))
			//);
			new SearchCatalogTree(myRootTree, mySearchItem);
			// normal catalog items to be inserted here
			new ManageCatalogsItemTree(myRootTree);
			new AddCustomCatalogItemTree(myRootTree);
		}

		boolean changedCatalogsList = false;
		int index = 1;
		for (INetworkLink link : activeLinks()) {
			final List<NetworkCatalogTree> trees = linkToTreeMap.remove(link);
			if (trees != null) {
				for (NetworkCatalogTree t : trees) {
					myRootTree.moveSubtree(t, index++);
				}
			} else {
				new NetworkCatalogRootTree(myRootTree, link, index++);
				changedCatalogsList = true;
			}
		}

		for (List<NetworkCatalogTree> trees : linkToTreeMap.values()) {
			for (NetworkCatalogTree t : trees) {
				t.removeSelf();
				changedCatalogsList = true;
			}
		}

		if (changedCatalogsList) {
			mySearchItem.setPattern(null);
		}

		fireModelChangedEvent(ChangeListener.Code.SomeCode);
	}

	private void updateVisibility() {
		for (FBTree tree : myRootTree.subtrees()) {
			if (tree instanceof NetworkCatalogTree) {
				((NetworkCatalogTree)tree).updateVisibility();
			}
		}
		fireModelChangedEvent(ChangeListener.Code.SomeCode);
	}

	public void synchronize() {
		if (myChildrenAreInvalid) {
			myChildrenAreInvalid = false;
			makeUpToDate();
			makeUpToDateRootAll();
		}
		if (myUpdateVisibility) {
			myUpdateVisibility = false;
			updateVisibility();
		}
		fireModelChangedEvent(ChangeListener.Code.SomeCode);
	}

	public NetworkTree getRootTree() {
		return myRootTree;
	}

	public NetworkCatalogTree getFakeCatalogTree(NetworkCatalogItem item) {
		final String id = item.getStringId();
		for (FBTree tree : myFakeRootTree.subtrees()) {
			if (tree instanceof NetworkCatalogTree &&
				id.equals(tree.getUniqueKey().Id)) {
				return (NetworkCatalogTree)tree;
			}
		}
		return new NetworkCatalogTree(myFakeRootTree, item.Link, item, 0);
	}

	public NetworkTree getTreeByKey(NetworkTree.Key key) {
		if (key == null) {
			return null;
		}
		if (key.Parent == null) {
			if (key.equals(myRootTree.getUniqueKey())) {
				return myRootTree;
			}
			if (key.equals(myFakeRootTree.getUniqueKey())) {
				return myFakeRootTree;
			}
			return null;
		}
		final NetworkTree parentTree = getTreeByKey(key.Parent);
		if (parentTree == null) {
			return null;
		}
		return parentTree != null ? (NetworkTree)parentTree.getSubtree(key.Id) : null;
	}

	public void addCustomLink(ICustomNetworkLink link) {
		final int id = link.getId();
		if (id == ICustomNetworkLink.INVALID_ID) {
			synchronized (myLinks) {
				final INetworkLink existing = getLinkByUrl(link.getUrl(UrlInfo.Type.Catalog));
				if (existing == null) {
					myLinks.add(link);
				} else {
					fireModelChangedEvent(ChangeListener.Code.SomeCode);
					return;
				}
			}
		} else {
			synchronized (myLinks) {
				for (int i = myLinks.size() - 1; i >= 0; --i) {
					final INetworkLink l = myLinks.get(i);
					if (l instanceof ICustomNetworkLink && ((ICustomNetworkLink)l).getId() == id) {
						myLinks.set(i, link);
						break;
					}
				}
			}
		}
		NetworkDatabase.Instance().saveLink(link);
		invalidateChildren();
		fireModelChangedEvent(ChangeListener.Code.SomeCode);
	}

	public void removeCustomLink(ICustomNetworkLink link) {
		myLinks.remove(link);
		NetworkDatabase.Instance().deleteLink(link);
		invalidateChildren();
	}

	public void addChangeListener(ChangeListener listener) {
		myListeners.add(listener);
	}

	public void removeChangeListener(ChangeListener listener) {
		myListeners.remove(listener);
	}

	// TODO: change to private
	/*private*/ public void fireModelChangedEvent(ChangeListener.Code code, Object ... params) {
		synchronized (myListeners) {
			for (ChangeListener l : myListeners) {
				l.onLibraryChanged(code, params);
			}
		}
	}

	public final void storeLoader(NetworkTree tree, NetworkItemsLoader loader) {
		myLoaders.put(tree, loader);
	}

	public final NetworkItemsLoader getStoredLoader(NetworkTree tree) {
		return tree != null ? myLoaders.get(tree) : null;
	}

	public final void startLoading(NetworkCatalogItem item) {
		if (item != null) {
			item.UpdatingInProgress = true;
			fireModelChangedEvent(ChangeListener.Code.SomeCode);
		}
	}

	public final void stopLoading(NetworkCatalogItem item) {
		if (item != null) {
			item.UpdatingInProgress = false;
			fireModelChangedEvent(ChangeListener.Code.SomeCode);
		}
	}

	public boolean isLoadingInProgress(NetworkTree tree) {
		return
			(tree instanceof NetworkCatalogTree &&
				((NetworkCatalogTree)tree).Item.UpdatingInProgress) ||
			getStoredLoader(tree) != null;
	}

	public final void removeStoredLoader(NetworkTree tree) {
		myLoaders.remove(tree);
	}

	public ZLImage getImageByUrl(String url, MimeType mimeType) {
		synchronized (myImageMap) {
			final WeakReference<ZLImage> ref = myImageMap.get(url);
			if (ref != null) {
				final ZLImage image = ref.get();
				if (image != null) {
					return image;
				}
			}
			final ZLImage image = new NetworkImage(url, SystemInfo);
			myImageMap.put(url, new WeakReference<ZLImage>(image));
			return image;
		}
	}
}
