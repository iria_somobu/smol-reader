/*
 * Copyright (C) 2010-2015 FBReader.ORG Limited <contact@fbreader.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

package org.geometerplus.fbreader.network.tree;

import org.geometerplus.fbreader.network.NetworkLibrary;
import org.geometerplus.zlibrary.core.network.ZLNetworkContext;
import org.geometerplus.zlibrary.core.network.ZLNetworkException;

public class CatalogExpander extends NetworkItemsLoader {
	private final boolean myResumeNotLoad;

	public CatalogExpander(ZLNetworkContext nc, NetworkCatalogTree tree, boolean authenticate, boolean resumeNotLoad) {
		super(nc, tree);
		myResumeNotLoad = resumeNotLoad;
	}

	@Override
	public void doBefore() throws ZLNetworkException {

	}

	@Override
	public void load() throws ZLNetworkException {
		if (myResumeNotLoad) {
			Tree.Item.resumeLoading(this);
		} else {
			Tree.Item.loadChildren(this);
		}
	}

	@Override
	protected void onFinish(ZLNetworkException exception, boolean interrupted) {
		if (interrupted && (!Tree.Item.supportsResumeLoading() || exception != null)) {
			Tree.clearCatalog();
		} else {
			Tree.removeUnconfirmedItems();
			if (!interrupted) {
				if (exception != null) {
					Tree.Library.fireModelChangedEvent(
						NetworkLibrary.ChangeListener.Code.NetworkError, exception.getMessage()
					);
				} else {
					Tree.updateLoadedTime();
					if (Tree.subtrees().isEmpty()) {
						Tree.Library.fireModelChangedEvent(
							NetworkLibrary.ChangeListener.Code.EmptyCatalog
						);
					}
				}
			}
			Tree.Library.invalidateVisibility();
			Tree.Library.synchronize();
		}
	}
}
