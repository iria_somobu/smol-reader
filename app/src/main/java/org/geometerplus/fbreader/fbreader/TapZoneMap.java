/*
 * Copyright (C) 2007-2015 FBReader.ORG Limited <contact@fbreader.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

package org.geometerplus.fbreader.fbreader;

import org.geometerplus.zlibrary.core.filesystem.ZLFile;
import org.geometerplus.zlibrary.core.options.ZLStringOption;
import org.geometerplus.zlibrary.core.util.XmlUtil;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class TapZoneMap {
	private static final List<String> ourPredefinedMaps = new LinkedList<String>();
	static {
		ourPredefinedMaps.add("right_to_left");
		ourPredefinedMaps.add("left_to_right");
	}

	private static final Map<String,TapZoneMap> ourMaps = new HashMap<String,TapZoneMap>();

	public static TapZoneMap zoneMap(String name) {
		TapZoneMap map = ourMaps.get(name);
		if (map == null) {
			map = new TapZoneMap(name);
			ourMaps.put(name, map);
		}
		return map;
	}

	public enum Tap {
		singleTap,
		singleNotDoubleTap,
		doubleTap
	};

	public final String Name;
	private final String myOptionGroupName;
	private final int myHeight = 3;
	private final int myWidth = 3;
	private final HashMap<Zone,ZLStringOption> myZoneMap = new HashMap<Zone,ZLStringOption>();
	private final HashMap<Zone,ZLStringOption> myZoneMap2 = new HashMap<Zone,ZLStringOption>();

	private TapZoneMap(String name) {
		Name = name;
		myOptionGroupName = "TapZones:" + name;

		final ZLFile mapFile = ZLFile.createFileByPath(
			"default/tapzones/" + name.toLowerCase() + ".xml"
		);

		XmlUtil.parseQuietly(mapFile, new Reader());
	}

	public boolean isCustom() {
		return !ourPredefinedMaps.contains(Name);
	}

	public int getHeight() {
		return myHeight;
	}

	public int getWidth() {
		return myWidth;
	}

	public String getActionByCoordinates(int x, int y, int width, int height, Tap tap) {
		if (width == 0 || height == 0) {
			return null;
		}
		x = Math.max(0, Math.min(width - 1, x));
		y = Math.max(0, Math.min(height - 1, y));
		return getActionByZone(myWidth * x / width, myHeight * y / height, tap);
	}

	public String getActionByZone(int h, int v, Tap tap) {
		final ZLStringOption option = getOptionByZone(new Zone(h, v), tap);
		return option != null ? option.getValue() : null;
	}

	private ZLStringOption getOptionByZone(Zone zone, Tap tap) {
		switch (tap) {
			default:
				return null;
			case singleTap:
			{
				final ZLStringOption option = myZoneMap.get(zone);
				return option != null ? option : myZoneMap2.get(zone);
			}
			case singleNotDoubleTap:
				return myZoneMap.get(zone);
			case doubleTap:
				return myZoneMap2.get(zone);
		}
	}

	private ZLStringOption createOptionForZone(Zone zone, boolean singleTap, String action) {
		return new ZLStringOption(
			myOptionGroupName,
			(singleTap ? "Action" : "Action2") + ":" + zone.HIndex + ":" + zone.VIndex,
			action
		);
	}

	private static class Zone {
		int HIndex;
		int VIndex;

		Zone(int h, int v) {
			HIndex = h;
			VIndex = v;
		}

		@Override
		public boolean equals(Object o) {
			if (o == this) {
				return true;
			}

			if (!(o instanceof Zone)) {
				return false;
			}

			final Zone tz = (Zone)o;
			return HIndex == tz.HIndex && VIndex == tz.VIndex;
		}

		@Override
		public int hashCode() {
			return (HIndex << 5) + VIndex;
		}
	}

	private class Reader extends DefaultHandler {
		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
			try {
				if ("zone".equals(localName)) {
					final Zone zone = new Zone(
						Integer.parseInt(attributes.getValue("x")),
						Integer.parseInt(attributes.getValue("y"))
					);
					final String action = attributes.getValue("action");
					final String action2 = attributes.getValue("action2");
					if (action != null) {
						myZoneMap.put(zone, createOptionForZone(zone, true, action));
					}
					if (action2 != null) {
						myZoneMap2.put(zone, createOptionForZone(zone, false, action2));
					}
				}
			} catch (Throwable e) {
			}
		}
	}
}
