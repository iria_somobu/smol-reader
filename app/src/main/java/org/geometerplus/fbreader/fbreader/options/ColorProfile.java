/*
 * Copyright (C) 2007-2015 FBReader.ORG Limited <contact@fbreader.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

package org.geometerplus.fbreader.fbreader.options;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import org.geometerplus.zlibrary.core.util.ZLColor;
import org.geometerplus.zlibrary.core.view.ZLPaintContext;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class ColorProfile {
    public static final String DAY = "DAY";
    public static final String NIGHT = "NIGHT";

    private static final String[] ourNames = { DAY, NIGHT };
    private static final HashMap<String, ColorProfile> ourProfiles = new HashMap<>();

    public static List<String> names() {
        return Collections.unmodifiableList(Arrays.asList(ourNames));
    }

    public static void uncache() {
        ourProfiles.clear();
    }

    public static ColorProfile get(Context ctx, String name) {
        ColorProfile profile = ourProfiles.get(name);
        if (profile == null) {
            profile = new ColorProfile(ctx, name);
            ourProfiles.put(name, profile);
        }
        return profile;
    }

    public final String Name;

    public String WallpaperOption;
    public final ZLPaintContext.FillMode FillModeOption = ZLPaintContext.FillMode.tile;
    public final int BackgroundOption;
    public final int SelectionBackgroundOption;
    public final int SelectionForegroundOption;
    public final int HighlightingForegroundOption;
    public final int HighlightingBackgroundOption;
    public final int RegularTextOption;
    public final int HyperlinkTextOption;
    public final int VisitedHyperlinkTextOption;

    private int get(SharedPreferences prefs, String group, String id) {
        return prefs.getInt(group + ":" + id, -1);
    }

    private int get(SharedPreferences prefs, String group, String id, int r, int g, int b) {
        return prefs.getInt(group + ":" + id, new ZLColor(r, g, b).intValue());
    }

    private ColorProfile(Context ctx, String name) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        Name = name;
        if (NIGHT.equals(name)) {
            WallpaperOption = prefs.getString(name + ":BG", "");
            BackgroundOption = get(prefs, name, "BG_COLOR", 0, 0, 0);
            SelectionBackgroundOption = get(prefs, name, "SELECTION_BG", 82, 131, 194);
            SelectionForegroundOption = get(prefs, name, "SELECTION_FG");
            HighlightingBackgroundOption = get(prefs, name, "SEARCH_BG", 96, 96, 128);
            HighlightingForegroundOption = get(prefs, name, "SEARCH_FG");
            RegularTextOption = get(prefs, name, "REGULAR", 192, 192, 192);
            HyperlinkTextOption = get(prefs, name, "HYPERLINK", 60, 142, 224);
        } else {
            WallpaperOption = prefs.getString(name + ":BG", "sepia");
            BackgroundOption = get(prefs, name, "BG_COLOR", 255, 255, 255);
            SelectionBackgroundOption = get(prefs, name, "SELECTION_BG", 82, 131, 194);
            SelectionForegroundOption = get(prefs, name, "SELECTION_FG");
            HighlightingBackgroundOption = get(prefs, name, "SEARCH_BG", 255, 192, 128);
            HighlightingForegroundOption = get(prefs, name, "SEARCH_FG");
            RegularTextOption = get(prefs, name, "REGULAR", 0, 0, 0);
            HyperlinkTextOption = get(prefs, name, "HYPERLINK", 60, 139, 255);
        }

        VisitedHyperlinkTextOption = get(prefs, name, "VISITED", 200, 139, 255);

        if (!WallpaperOption.isEmpty()) {
            WallpaperOption = "wallpapers/" + WallpaperOption + ".jpg";
        }
    }
}
