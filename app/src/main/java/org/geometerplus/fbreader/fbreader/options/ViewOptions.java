/*
 * Copyright (C) 2007-2015 FBReader.ORG Limited <contact@fbreader.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

package org.geometerplus.fbreader.fbreader.options;

import android.content.Context;
import android.preference.PreferenceManager;

import org.geometerplus.zlibrary.core.library.ZLibrary;
import org.geometerplus.zlibrary.text.view.style.ZLTextStyleCollection;

public class ViewOptions {
    private final Context ctx;

    private ColorProfile myColorProfile;
    private ZLTextStyleCollection myTextStyleCollection;

    private final boolean defaultTwoColumn;

    public ViewOptions(Context ctx) {
        this.ctx = ctx;
        final ZLibrary zlibrary = ZLibrary.Instance();

        final int dpi = zlibrary.getDisplayDPI();
        final int x = zlibrary.getWidthInPixels();
        final int y = zlibrary.getHeightInPixels();

        defaultTwoColumn = x * x + y * y >= 42 * dpi * dpi;
    }

    public String getColorProfileName() {
        return PreferenceManager.getDefaultSharedPreferences(ctx)
                .getString("CURRENT_PROFILE", ColorProfile.DAY);
    }

    public void setColorProfileName(String profileName) {
        PreferenceManager.getDefaultSharedPreferences(ctx)
                .edit()
                .putString("CURRENT_PROFILE", profileName)
                .apply();
    }

    public boolean isTwoColumnView() {
        return PreferenceManager.getDefaultSharedPreferences(ctx)
                .getBoolean("APPEAR_TWO_COLUMN", defaultTwoColumn);
    }

    public ColorProfile getColorProfile() {
        final String name = getColorProfileName();
        if (myColorProfile == null || !name.equals(myColorProfile.Name)) {
            myColorProfile = ColorProfile.get(ctx, name);
        }
        return myColorProfile;
    }

    public ZLTextStyleCollection getTextStyleCollection() {
        if (myTextStyleCollection == null) {
            myTextStyleCollection = new ZLTextStyleCollection(ctx);
        }
        return myTextStyleCollection;
    }

    public void reloadTextStyleCollection() {
        myTextStyleCollection = new ZLTextStyleCollection(ctx);

        // Reload color profile too
        myColorProfile = null;
        ColorProfile.uncache();
        getColorProfile();
    }
}
