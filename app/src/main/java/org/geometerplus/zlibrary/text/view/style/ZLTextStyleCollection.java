/*
 * Copyright (C) 2007-2015 FBReader.ORG Limited <contact@fbreader.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

package org.geometerplus.zlibrary.text.view.style;

import android.content.Context;

import com.somobu.smolreader.prefs.StylesHelper;

import java.util.*;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import org.geometerplus.zlibrary.core.filesystem.ZLResourceFile;
import org.geometerplus.zlibrary.core.util.XmlUtil;
import org.geometerplus.zlibrary.text.model.ZLTextAlignmentType;

public class ZLTextStyleCollection {

    private final ZLTextBaseStyle myBaseStyle;
    private final ZLTextNGStyleDescription[] myDescriptionMap = new ZLTextNGStyleDescription[256];

    public ZLTextStyleCollection(Context ctx) {

        myBaseStyle = new ZLTextBaseStyle(ctx);

        StylesHelper styles = new StylesHelper();
        Map<String, Map<String, String>> defaults = styles.getDefault();

        for (Map.Entry<String, Map<String, String>> entry : defaults.entrySet()) {
            Map<String, String> currentMap = entry.getValue();

            int key = Integer.parseInt(currentMap.get("fbreader-id"));
            ZLTextNGStyleDescription v = new ZLTextNGStyleDescription(ctx, entry.getKey(), currentMap);

            myDescriptionMap[key & 0xFF] = v;
        }

    }

    public ZLTextBaseStyle getBaseStyle() {
        return myBaseStyle;
    }

    public ZLTextNGStyleDescription getDescription(byte kind) {
        return myDescriptionMap[kind & 0xFF];
    }

}
