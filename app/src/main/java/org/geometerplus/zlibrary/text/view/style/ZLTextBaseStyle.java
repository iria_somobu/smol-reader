/*
 * Copyright (C) 2007-2015 FBReader.ORG Limited <contact@fbreader.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

package org.geometerplus.zlibrary.text.view.style;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.somobu.smolreader.prefs.CachedPrefsStore;

import org.geometerplus.zlibrary.core.fonts.FontEntry;
import org.geometerplus.zlibrary.core.library.ZLibrary;
import org.geometerplus.zlibrary.text.model.ZLTextAlignmentType;
import org.geometerplus.zlibrary.text.model.ZLTextMetrics;
import org.geometerplus.zlibrary.text.view.ZLTextHyperlink;
import org.geometerplus.zlibrary.text.view.ZLTextStyle;

import java.util.Collections;
import java.util.List;

public class ZLTextBaseStyle extends ZLTextStyle {
    public CachedPrefsStore prefs;

    public final boolean AutoHyphenationOption;

    public final boolean BoldOption;
    public final boolean ItalicOption;
    public final boolean UnderlineOption;
    public final boolean StrikeThroughOption;
    public final int AlignmentOption;
    public final int LineSpaceOption;

    public final String FontFamilyOption;
    public final int FontSizeOption;

    public ZLTextBaseStyle(Context ctx) {
        super(null, ZLTextHyperlink.NO_LINK);
        prefs = CachedPrefsStore.instance();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);

        float fontCoef = ZLibrary.Instance().getDisplayDPI() / 160.0f;
        FontSizeOption = (int) (Integer.parseInt(prefs.getString("CONTENT_FONT_SIZE", "18")) * fontCoef);
        AutoHyphenationOption = prefs.getBoolean("CONTENT_AUTO_HYPHEN", true);
        FontFamilyOption = prefs.getString("CONTENT_FONT", "Droid Sans");

        String style = prefs.getString("CONTENT_FONT_STYLE", "regular");
        BoldOption = style.equals("bold") || style.equals("bold_italic");
        ItalicOption = style.equals("italic") || style.equals("bold_italic");
        UnderlineOption = false;
        StrikeThroughOption = false;

        LineSpaceOption = (int) (Float.parseFloat(prefs.getString("CONTENT_LINE_SPACING", "1.2")) * 10);

        switch (prefs.getString("CONTENT_ALIGNMENT", "justify")) {
            case "left":
                AlignmentOption = ZLTextAlignmentType.ALIGN_LEFT;
                break;
            case "right":
                AlignmentOption = ZLTextAlignmentType.ALIGN_RIGHT;
                break;
            case "center":
                AlignmentOption = ZLTextAlignmentType.ALIGN_CENTER;
                break;
            case "justify":
                AlignmentOption = ZLTextAlignmentType.ALIGN_JUSTIFY;
                break;
            default:
                AlignmentOption = ZLTextAlignmentType.ALIGN_UNDEFINED;
                break;
        }
    }

    private String myFontFamily;
    private List<FontEntry> myFontEntries;

    @Override
    public List<FontEntry> getFontEntries() {
        final String family = FontFamilyOption;
        if (myFontEntries == null || !family.equals(myFontFamily)) {
            myFontEntries = Collections.singletonList(FontEntry.systemEntry(family));
        }
        return myFontEntries;
    }

    public int getFontSize() {
        return FontSizeOption;
    }

    @Override
    public int getFontSize(ZLTextMetrics metrics) {
        return getFontSize();
    }

    @Override
    public boolean isBold() {
        return BoldOption;
    }

    @Override
    public boolean isItalic() {
        return ItalicOption;
    }

    @Override
    public boolean isUnderline() {
        return UnderlineOption;
    }

    @Override
    public boolean isStrikeThrough() {
        return StrikeThroughOption;
    }

    @Override
    public int getLeftMargin(ZLTextMetrics metrics) {
        return 0;
    }

    @Override
    public int getRightMargin(ZLTextMetrics metrics) {
        return 0;
    }

    @Override
    public int getLeftPadding(ZLTextMetrics metrics) {
        return 0;
    }

    @Override
    public int getRightPadding(ZLTextMetrics metrics) {
        return 0;
    }

    @Override
    public int getFirstLineIndent(ZLTextMetrics metrics) {
        return 0;
    }

    @Override
    public int getLineSpacePercent() {
        return LineSpaceOption * 10;
    }

    @Override
    public int getVerticalAlign(ZLTextMetrics metrics) {
        return 0;
    }

    @Override
    public boolean isVerticallyAligned() {
        return false;
    }

    @Override
    public int getSpaceBefore(ZLTextMetrics metrics) {
        return 0;
    }

    @Override
    public int getSpaceAfter(ZLTextMetrics metrics) {
        return 0;
    }

    @Override
    public byte getAlignment() {
        return (byte) AlignmentOption;
    }

    @Override
    public boolean allowHyphenations() {
        return true;
    }
}
