/*
 * Copyright (C) 2007-2015 FBReader.ORG Limited <contact@fbreader.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

package org.geometerplus.zlibrary.text.view.style;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.somobu.smolreader.prefs.StylesHelper;

import org.geometerplus.zlibrary.core.util.ZLBoolean3;
import org.geometerplus.zlibrary.text.model.ZLTextAlignmentType;
import org.geometerplus.zlibrary.text.model.ZLTextMetrics;
import org.geometerplus.zlibrary.text.model.ZLTextStyleEntry;

import java.util.HashMap;
import java.util.Map;

public class ZLTextNGStyleDescription {
    public final String Name;

    public final String FontFamilyOption;
    public final String FontSizeOption;
    public final String FontWeightOption;
    public final String FontStyleOption;
    public final String TextDecorationOption;
    public final String HyphenationOption;
    public final String MarginTopOption;
    public final String MarginBottomOption;
    public final String MarginLeftOption;
    public final String MarginRightOption;
    public final String TextIndentOption;
    public final String AlignmentOption;
    public final String VerticalAlignOption;
    public final String LineHeightOption;

    private String getValue(SharedPreferences prefs, String selector, String name, Map<String, String> valueMap) {
        String defaultValue = valueMap.get(name);
        if (defaultValue == null) defaultValue = "";
        return prefs.getString(selector + "::" + name, defaultValue);
    }

    ZLTextNGStyleDescription(Context ctx, String selector, Map<String, String> valueMap) {
        Name = valueMap.get(StylesHelper.name);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        FontFamilyOption = getValue(prefs, selector, StylesHelper.font_family, valueMap);
        FontSizeOption = getValue(prefs, selector, StylesHelper.font_size, valueMap);
        FontWeightOption = getValue(prefs, selector, StylesHelper.font_weight, valueMap);
        FontStyleOption = getValue(prefs, selector, StylesHelper.font_style, valueMap);
        TextDecorationOption = getValue(prefs, selector, StylesHelper.text_decoration, valueMap);
        HyphenationOption = getValue(prefs, selector, StylesHelper.hyphens, valueMap);
        MarginTopOption = getValue(prefs, selector, StylesHelper.margin_top, valueMap);
        MarginBottomOption = getValue(prefs, selector, StylesHelper.margin_bottom, valueMap);
        MarginLeftOption = getValue(prefs, selector, StylesHelper.margin_left, valueMap);
        MarginRightOption = getValue(prefs, selector, StylesHelper.margin_right, valueMap);
        TextIndentOption = getValue(prefs, selector, StylesHelper.text_ident, valueMap);
        AlignmentOption = getValue(prefs, selector, StylesHelper.text_align, valueMap);
        VerticalAlignOption = getValue(prefs, selector, StylesHelper.vertical_align, valueMap);
        LineHeightOption = getValue(prefs, selector, StylesHelper.line_height, valueMap);
    }

    int getFontSize(ZLTextMetrics metrics, int parentFontSize) {
        final ZLTextStyleEntry.Length length = parseLength(FontSizeOption);
        if (length == null) {
            return parentFontSize;
        }
        return ZLTextStyleEntry.compute(
                length, metrics, parentFontSize, ZLTextStyleEntry.Feature.LENGTH_FONT_SIZE
        );
    }

    int getVerticalAlign(ZLTextMetrics metrics, int base, int fontSize) {
        final ZLTextStyleEntry.Length length = parseLength(VerticalAlignOption);
        if (length == null) {
            return base;
        }
        return ZLTextStyleEntry.compute(
                // TODO: add new length for vertical alignment
                length, metrics, fontSize, ZLTextStyleEntry.Feature.LENGTH_FONT_SIZE
        );
    }

    boolean hasNonZeroVerticalAlign() {
        final ZLTextStyleEntry.Length length = parseLength(VerticalAlignOption);
        return length != null && length.Size != 0;
    }

    int getLeftMargin(ZLTextMetrics metrics, int base, int fontSize) {
        final ZLTextStyleEntry.Length length = parseLength(MarginLeftOption);
        if (length == null) {
            return base;
        }
        return base + ZLTextStyleEntry.compute(
                length, metrics, fontSize, ZLTextStyleEntry.Feature.LENGTH_MARGIN_LEFT
        );
    }

    int getRightMargin(ZLTextMetrics metrics, int base, int fontSize) {
        final ZLTextStyleEntry.Length length = parseLength(MarginRightOption);
        if (length == null) {
            return base;
        }
        return base + ZLTextStyleEntry.compute(
                length, metrics, fontSize, ZLTextStyleEntry.Feature.LENGTH_MARGIN_RIGHT
        );
    }

    int getLeftPadding(ZLTextMetrics metrics, int base, int fontSize) {
        return base;
    }

    int getRightPadding(ZLTextMetrics metrics, int base, int fontSize) {
        return base;
    }

    int getFirstLineIndent(ZLTextMetrics metrics, int base, int fontSize) {
        final ZLTextStyleEntry.Length length = parseLength(TextIndentOption);
        if (length == null) {
            return base;
        }
        return ZLTextStyleEntry.compute(
                length, metrics, fontSize, ZLTextStyleEntry.Feature.LENGTH_FIRST_LINE_INDENT
        );
    }

    int getSpaceBefore(ZLTextMetrics metrics, int base, int fontSize) {
        final ZLTextStyleEntry.Length length = parseLength(MarginTopOption);
        if (length == null) {
            return base;
        }
        return ZLTextStyleEntry.compute(
                length, metrics, fontSize, ZLTextStyleEntry.Feature.LENGTH_SPACE_BEFORE
        );
    }

    int getSpaceAfter(ZLTextMetrics metrics, int base, int fontSize) {
        final ZLTextStyleEntry.Length length = parseLength(MarginBottomOption);
        if (length == null) {
            return base;
        }
        return ZLTextStyleEntry.compute(
                length, metrics, fontSize, ZLTextStyleEntry.Feature.LENGTH_SPACE_AFTER
        );
    }

    ZLBoolean3 isBold() {
        final String fontWeight = FontWeightOption;
        if ("bold".equals(fontWeight)) {
            return ZLBoolean3.B3_TRUE;
        } else if ("normal".equals(fontWeight)) {
            return ZLBoolean3.B3_FALSE;
        } else {
            return ZLBoolean3.B3_UNDEFINED;
        }
    }

    ZLBoolean3 isItalic() {
        final String fontStyle = FontStyleOption;
        if ("italic".equals(fontStyle) || "oblique".equals(fontStyle)) {
            return ZLBoolean3.B3_TRUE;
        } else if ("normal".equals(fontStyle)) {
            return ZLBoolean3.B3_FALSE;
        } else {
            return ZLBoolean3.B3_UNDEFINED;
        }
    }

    ZLBoolean3 isUnderlined() {
        final String textDecoration = TextDecorationOption;
        if ("underline".equals(textDecoration)) {
            return ZLBoolean3.B3_TRUE;
        } else if ("".equals(textDecoration) || "inherit".equals(textDecoration)) {
            return ZLBoolean3.B3_UNDEFINED;
        } else {
            return ZLBoolean3.B3_FALSE;
        }
    }

    ZLBoolean3 isStrikedThrough() {
        final String textDecoration = TextDecorationOption;
        if ("line-through".equals(textDecoration)) {
            return ZLBoolean3.B3_TRUE;
        } else if ("".equals(textDecoration) || "inherit".equals(textDecoration)) {
            return ZLBoolean3.B3_UNDEFINED;
        } else {
            return ZLBoolean3.B3_FALSE;
        }
    }

    byte getAlignment() {
        final String alignment = AlignmentOption;
        if (alignment.length() == 0) {
            return ZLTextAlignmentType.ALIGN_UNDEFINED;
        } else if ("center".equals(alignment)) {
            return ZLTextAlignmentType.ALIGN_CENTER;
        } else if ("left".equals(alignment)) {
            return ZLTextAlignmentType.ALIGN_LEFT;
        } else if ("right".equals(alignment)) {
            return ZLTextAlignmentType.ALIGN_RIGHT;
        } else if ("justify".equals(alignment)) {
            return ZLTextAlignmentType.ALIGN_JUSTIFY;
        } else {
            return ZLTextAlignmentType.ALIGN_UNDEFINED;
        }
    }

    ZLBoolean3 allowHyphenations() {
        final String hyphen = HyphenationOption;
        if ("auto".equals(hyphen)) {
            return ZLBoolean3.B3_TRUE;
        } else if ("none".equals(hyphen)) {
            return ZLBoolean3.B3_FALSE;
        } else {
            return ZLBoolean3.B3_UNDEFINED;
        }
    }

    private static final Map<String, Object> ourCache = new HashMap<String, Object>();
    private static final Object ourNullObject = new Object();

    private static ZLTextStyleEntry.Length parseLength(String value) {
        if (value.length() == 0) {
            return null;
        }

        final Object cached = ourCache.get(value);
        if (cached != null) {
            return cached == ourNullObject ? null : (ZLTextStyleEntry.Length) cached;
        }

        ZLTextStyleEntry.Length length = null;
        try {
            if (value.endsWith("%")) {
                length = new ZLTextStyleEntry.Length(
                        Short.parseShort(value.substring(0, value.length() - 1)),
                        ZLTextStyleEntry.SizeUnit.PERCENT
                );
            } else if (value.endsWith("rem")) {
                length = new ZLTextStyleEntry.Length(
                        (short) (100 * Double.parseDouble(value.substring(0, value.length() - 2))),
                        ZLTextStyleEntry.SizeUnit.REM_100
                );
            } else if (value.endsWith("em")) {
                length = new ZLTextStyleEntry.Length(
                        (short) (100 * Double.parseDouble(value.substring(0, value.length() - 2))),
                        ZLTextStyleEntry.SizeUnit.EM_100
                );
            } else if (value.endsWith("ex")) {
                length = new ZLTextStyleEntry.Length(
                        (short) (100 * Double.parseDouble(value.substring(0, value.length() - 2))),
                        ZLTextStyleEntry.SizeUnit.EX_100
                );
            } else if (value.endsWith("px")) {
                length = new ZLTextStyleEntry.Length(
                        Short.parseShort(value.substring(0, value.length() - 2)),
                        ZLTextStyleEntry.SizeUnit.PIXEL
                );
            } else if (value.endsWith("pt")) {
                length = new ZLTextStyleEntry.Length(
                        Short.parseShort(value.substring(0, value.length() - 2)),
                        ZLTextStyleEntry.SizeUnit.POINT
                );
            }
        } catch (Exception e) {
            // ignore
        }
        ourCache.put(value, length != null ? length : ourNullObject);
        return length;
    }
}
