/*
 * Copyright (C) 2010-2015 FBReader.ORG Limited <contact@fbreader.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

package org.geometerplus.android.fbreader.network;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.webkit.MimeTypeMap;

import org.geometerplus.android.util.UIUtil;
import org.geometerplus.fbreader.Paths;
import org.geometerplus.fbreader.network.INetworkLink;
import org.geometerplus.fbreader.network.NetworkBookItem;
import org.geometerplus.fbreader.network.NetworkLibrary;
import org.geometerplus.fbreader.network.urlInfo.BookUrlInfo;
import org.geometerplus.fbreader.network.urlInfo.UrlInfo;
import org.geometerplus.zlibrary.core.network.ZLNetworkContext;
import org.geometerplus.zlibrary.core.network.ZLNetworkException;
import org.geometerplus.zlibrary.core.options.Config;
import org.geometerplus.zlibrary.core.util.MimeType;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public abstract class Util {

	public static final String EDIT_CATALOG_ACTION = "android.fbreader.action.EDIT_OPDS_CATALOG";

	public static Intent intentByLink(Intent intent, INetworkLink link) {
		if (link != null) {
			intent.setData(Uri.parse(link.getUrl(UrlInfo.Type.Catalog)));
		}
		return intent;
	}

	public static NetworkLibrary networkLibrary(Context context) {
		return NetworkLibrary.Instance(Paths.systemInfo(context));
	}

	public static void initLibrary(final Activity activity, final ZLNetworkContext nc, final Runnable action) {
		Config.Instance().runOnConnect(new Runnable() {
			public void run() {
				UIUtil.wait("loadingNetworkLibrary", new Runnable() {
					public void run() {
						final NetworkLibrary library = networkLibrary(activity);
						if (SQLiteNetworkDatabase.Instance() == null) {
							new SQLiteNetworkDatabase(activity.getApplication(), library);
						}

						if (!library.isInitialized()) {
							try {
								library.initialize(nc);
							} catch (ZLNetworkException e) {
							}
						}
						if (action != null) {
							action.run();
						}
					}
				}, activity);
			}
		});
	}

	public static void openInBrowser(Activity activity, String url) {
		if (url != null) {
			url = networkLibrary(activity).rewriteUrl(url, true);
			activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
		}
	}

	public static void doDownloadBook(Activity activity, final NetworkBookItem book, boolean demo) {
		final UrlInfo.Type resolvedType = demo ? UrlInfo.Type.BookDemo : UrlInfo.Type.Book;
		final BookUrlInfo ref = book.reference(resolvedType);
		if (ref != null) {
			Paths.invalidatePathsCache(activity);

			String url = ref.Url;
			String destination = BookUrlInfo.makeBookFileName(url, ref.Mime, resolvedType);

			doDownloadBook(activity, url, "file://"+destination, book.Title.toString());
		}
	}

	public static void doDownloadBook(Context ctx, String url){
		UrlInfo.Type referenceType = UrlInfo.Type.Book;
		String destination = BookUrlInfo.makeBookFileName(url, MimeType.get(getMimeType(url)), referenceType);

		doDownloadBook(ctx, url, "file://"+destination, new File(destination).getName());
	}

	private static String getMimeType(String url) {
		String type = null;
		String extension = MimeTypeMap.getFileExtensionFromUrl(url);
		if (extension != null) {
			type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
		}
		return type;
	}

	private static void doDownloadBook(Context ctx, String src, String dst, String title){
		DownloadManager.Request rq = new DownloadManager.Request(Uri.parse(src));
		rq.setDestinationUri(Uri.parse(dst));
		rq.setTitle(title);

		DownloadManager mgr = (DownloadManager) ctx.getSystemService(Context.DOWNLOAD_SERVICE);
		mgr.enqueue(rq);
	}

	public static boolean isDownloadableUri(Uri uri, String mime) {
		final List<String> path = uri.getPathSegments();
		if (path == null || path.isEmpty()) {
			return false;
		}

		final String scheme = uri.getScheme();
		if ("epub".equals(scheme) || "book".equals(scheme)) {
			return true;
		}

		if (mime != null && Arrays.asList(new String[] {
				"application/epub+zip",
				"application/x-pilot-prc",
				"application/x-mobipocket-ebook",
				"application/x-fictionbook+xml",
				"application/x-fictionbook",
		}).contains(mime)) {
			return true;
		}

		final String fileName = path.get(path.size() - 1).toLowerCase();
		return
				fileName.endsWith(".fb2.zip") ||
						fileName.endsWith(".fb2") ||
						fileName.endsWith(".epub") ||
						fileName.endsWith(".oeb") ||
						fileName.endsWith(".mobi") ||
						fileName.endsWith(".txt") ||
						fileName.endsWith(".rtf") ||
						fileName.endsWith(".prc");
	}

	public static Uri rewriteUri(Uri uri) {
		return uri;
	}
}
