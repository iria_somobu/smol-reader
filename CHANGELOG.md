# v 2.0 (WIP)

* Added:
  - Basic e-reader support;
  - Theme switcher;
* Changed/reworked:
  - Application icon was reverted to old one;
  - Book downloader was replaced w/ system DownloadManager;
  - Book info screen was slightly changed;
  - Bookmarks and TOC screens were reworked and merged into one tabbed screen;
  - Cancel menu was replaced w/ custom Back menu;
  - Library and network library were reworked and merged into one tabbed screen;
  - Settings were rewritten using SharedPreferences Android API;
* Removed:
  - Custom crash handlers;
  - Bookshelf and premium promotion;
  - 'Buy book' functionality;
  - External plugins & plugin API;
  - FBReader's proprietary cloud integration;
  - Litres integration;
  - Nanohttpd server and DataService;
  - Paragon Dictionary integration;
  - Translations (sorry);
  - Weird book formats support: pdf, djvu, comicbook
  - YOTA integration; 

